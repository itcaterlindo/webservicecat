﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceCAT.Models
{
    public class APDownPaymentModel
    {
        public string KdPurchaseOrder { get; set; }
        public string U_IDU_APDP_INTNUM { get; set; }
        public string U_FP { get; set; }
        public DateTime DocDate { get; set; }
        public double Amount { get; set; }
        public string U_IDU_WEBUSER { get; set; }
    }
}