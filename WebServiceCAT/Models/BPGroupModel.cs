﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceCAT.Models
{
    public class BPGroupModel
    {
        public string U_IDU_WEBID { get; set; }
        public string GroupName { get; set; }
        public string GroupType { get; set; }
    }
}