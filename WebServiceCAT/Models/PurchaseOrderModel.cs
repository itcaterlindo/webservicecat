﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceCAT.Models
{
    public class PurchaseOrderModel
    {
        public string U_IDU_WEBID { get; set; }
        public string U_IDU_PO_INTNUM { get; set; }
        public string U_IDU_PR_INTNUM { get; set; }
        public DateTime DocDate { get; set; }
        public string NumAtCard { get; set; }
        public string Comments { get; set; }
        public string CardCode { get; set; }
        public string U_IDU_WEBUSER { get; set; }

        public List<DetailPO> Lines_Detail_Item = new List<DetailPO>();
    }

    public class DetailPO
    {
        public string U_IDU_WEBID { get; set; }
        public string ItemCode { get; set; }
        public string Dscription { get; set; }
        public string U_IDU_Spesifikasi { get; set; }
        public string KdCurrency { get; set; }
        public double Quantity { get; set; }
        public double Konversi { get; set; }
        public string UomName { get; set; }
        public double LineTotal { get; set; }
        public DateTime ShipDate { get; set; }
    }
}