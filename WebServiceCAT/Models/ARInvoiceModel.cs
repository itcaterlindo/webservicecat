﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceCAT.Models
{
    public class ARInvoiceModel
    {
        public string U_IDU_WEBID { get; set; }
        public string KdSalesOrder { get; set; }
        public string U_IDU_AR_INTNUM { get; set; }
        public DateTime DocDate { get; set; }
        public DateTime U_IDU_Stuffing_Date { get; set; }
        public string NumAtCard { get; set; }
        public string Comments { get; set; }
        public string U_IDU_WEBUSER { get; set; }

        public List<DetailAR> Lines_Detail_Item = new List<DetailAR>();

    }

    public class DetailAR
    {
        public string U_IDU_WEBID { get; set; }
        public string KdSalesOrderDtl { get; set; }
        public double Quantity { get; set; }
        public string KdWarehouse { get; set; }
    }
}