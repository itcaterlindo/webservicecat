﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceCAT.Models
{
    public class JenisVendorModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string U_IDU_Tipe { get; set; }
        public string U_IDU_GroupNum { get; set; }
        public string U_IDU_VatGroup { get; set; }
    }
}