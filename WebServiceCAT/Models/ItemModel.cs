﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceCAT.Models
{
    public class ItemModel
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string PurchaseItem { get; set; }
        public string SalesItem { get; set; }
        public string InventoryItem { get; set; }
        public string KdItemGroup { get; set; }
        public string Dimensi { get; set; }
        public double NetWeight { get; set; }
        public double GrossWeight { get; set; }
        public double BoxWeight { get; set; }
        public double Length { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Volume { get; set; }
        public string PrchseItem { get; set; }
        public string SellItem { get; set; }
        public string InvntItem { get; set; }
        public int StockSafety { get; set; }
        public string U_IDU_WEBID { get; set; }
    }
}