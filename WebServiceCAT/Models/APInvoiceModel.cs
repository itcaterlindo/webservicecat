﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceCAT.Models
{
    public class APInvoiceModel
    {
        public string U_IDU_WEBID { get; set; }
        public string CardCode { get; set; }
        public string U_IDU_AP_INTNUM { get; set; }
        public string U_FP { get; set; }
        public DateTime DocDate { get; set; }
        public DateTime DocDueDate { get; set; }
        public string NumAtCard { get; set; }
        public double DiscPrcnt { get; set; }
        public string VatGroup { get; set; }
        public string Comments { get; set; }
        public string WTCode { get; set; }
        public string U_IDU_WEBUSER { get; set; }

        public List<DetailAP> Lines_Detail_Item = new List<DetailAP>();

    }

    public class DetailAP
    {
        public string U_IDU_WEBID { get; set; }
        public string KdGRPODtl { get; set; }
        public double Quantity { get; set; }
        public double Konversi { get; set; }
        public double LineTotal { get; set; }
        public string WithholdingTax { get; set; }
    }
}