﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceCAT.Models
{
    public class SalesPersonModel
    {
        public string U_IDU_WEBID { get; set; }
        public string SlpName { get; set; }
        public string Email { get; set; }
        public string Telp { get; set; }
    }
}