﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceCAT.Models
{
    public class SalesOrderModel
    {
        public string U_IDU_WEBID { get; set; }
        public string KdSalesPerson { get; set; }
        public string U_IDU_SO_INTNUM { get; set; }
        public DateTime DocDate { get; set; }
        public DateTime DocDueDate { get; set; }
        public string NumAtCard { get; set; }
        public string Comments { get; set; }
        public string U_IDU_PO_INTNUM { get; set; }
        public DateTime U_IDU_PO_Date { get; set; }
        public string CardCode { get; set; }
        public string Street { get; set; }
        public double OngkosKirim { get; set; }
        public double BiayaInstall { get; set; }
        public double DocTotal { get; set; }
        public string U_IDU_WEBUSER { get; set; }

        public List<DetailItem> Lines_Detail_Item = new List<DetailItem>();

        public List<SubDetailItem> Lines_Sub_Detail_Item = new List<SubDetailItem>();
    }

    public class DetailItem
    {
        public string U_IDU_WEBID { get; set; }
        public string ItemCode { get; set; }
        public string Dscription { get; set; }
        public string KdWarehouse { get; set; }
        public string KdCurrency { get; set; }
        public double Quantity { get; set; }
        public string KdSatuan { get; set; }
        public double UnitPrice { get; set; }
        public string DiscType { get; set; }
        public double ItemDisc { get; set; }
        public double DiscPrcnt { get; set; }
        public double LineTotal { get; set; }
        public string U_IDU_FreeText { get; set; }
        public string U_IDU_OrderType { get; set; }
    }

    public class SubDetailItem
    {
        public string U_IDU_WEBID { get; set; }
        public string ItemCode { get; set; }
        public string Dscription { get; set; }
        public string KdWarehouse { get; set; }
        public string KdCurrency { get; set; }
        public double Quantity { get; set; }
        public string KdSatuan { get; set; }
        public double UnitPrice { get; set; }
        public string DiscType { get; set; }
        public double ItemDisc { get; set; }
        public double DiscPrcnt { get; set; }
        public double LineTotal { get; set; }
        public string U_IDU_FreeText { get; set; }
        public string U_IDU_OrderType { get; set; }
    }
}