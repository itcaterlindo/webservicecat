﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceCAT.Models
{
    public class BusinessPartnerModel
    {
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public string CardType { get; set; }
        public string KdGroup { get; set; }
        public string CntctPrsn { get; set; }
        public string LicTradNum { get; set; }
        public string DebPayAcct { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string Currency { get; set; }
        public string Street { get; set; }
        public string ZipCode { get; set; }
        public string VendorGroupNum { get; set; }
        public string VendorVatGroup { get; set; }
        public string U_IDU_WEBID { get; set; }
        public string U_IDU_WEBUSER { get; set; }
        public string U_IDU_KodeJenisCustomer { get; set; }
        public string U_IDU_BadanUsaha { get; set; }

        public List<BP_ShipTo> Lines_ShipTo = new List<BP_ShipTo>();
    }

    public class BP_ShipTo
    {
        public string Street { get; set; }
        public string ZipCode { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string U_IDU_BadanUsaha { get; set; }
        public string U_IDU_NamaCustomer { get; set; }
        public string U_IDU_CP { get; set; }
    }
}