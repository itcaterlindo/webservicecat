﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceCAT.Models
{
    public class ARDownPaymentModel
    {
        public string KdSalesOrder { get; set; }
        public string U_IDU_ARDP_INTNUM { get; set; }
        public string NumAtCard { get; set; }
        public DateTime DocDate { get; set; }
        public double Amount { get; set; }
        public string U_IDU_WEBUSER { get; set; }
    }
}