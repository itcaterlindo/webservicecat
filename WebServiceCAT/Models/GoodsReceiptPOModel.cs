﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceCAT.Models
{
    public class GoodsReceiptPOModel
    {
        public string U_IDU_WEBID { get; set; }
        public DateTime DocDate { get; set; }
        public string NumAtCard { get; set; }
        public string Comments { get; set; }
        public string U_IDU_NoSJ { get; set; }
        public DateTime U_IDU_TglSJ { get; set; }
        public string U_IDU_WEBUSER { get; set; }

        public List<DetailGRPO> Lines_Detail_Item = new List<DetailGRPO>();
    }

    public class DetailGRPO
    {
        public string U_IDU_WEBID { get; set; }
        public string KdPurchaseOrderDtl { get; set; }
        public double Quantity { get; set; }
        public double Konversi { get; set; }
        public string KdWarehouse { get; set; }
    }
}