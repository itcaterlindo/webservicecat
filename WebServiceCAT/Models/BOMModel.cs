﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceCAT.Models
{
    public class BOM_Header
    {
        public string U_IDU_WEBID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public double Quantity { get; set; }
        public string TreeType { get; set; }
        public string Warehouse { get; set; }

        public List<BOM_Detail> Lines = new List<BOM_Detail>();
    }

    public class BOM_Detail
    {
        public string RouteStage { get; set; }
        public string ItemType { get; set; }
        public string ItemCode { get; set; }
        public double Quantity { get; set; }
        public string Warehouse { get; set; }
        public string IssueMethod { get; set; }

    }

}