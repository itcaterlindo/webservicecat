﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceCAT.Models
{
    public class SearchModel
    {
        public string CustomQuery { get; set; }
        public string Select { get; set; }
        public string From { get; set; }
        public string Where { get; set; }
        public string OrderBy { get; set; }
    }
}