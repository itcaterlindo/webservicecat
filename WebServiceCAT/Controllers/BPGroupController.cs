﻿using SAPbobsCOM;
using System;
using System.Net;
using System.Web.Http;
using WebServiceCAT.Models;
using WebServiceCAT.SAP;

namespace WebServiceCAT.Controllers
{
    public class BPGroupController : ApiController
    {
        [HttpPost]
        [Route("api/AddBPGroup")]
        public MessageInfo AddBPGroup([FromBody] BPGroupModel model)
        {
            string funcname = "AddBPGroup";
            MessageInfo ret = null;
            Company oCompany = new Company();

            try
            {
                oCompany = Connection.GetCompany();
                return ret = BPGroup.AddBPGroup(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/EditBPGroup")]
        public MessageInfo EditBPGroup([FromBody] BPGroupModel model)
        {
            string funcname = "EditBPGroup";
            MessageInfo ret = null;
            Company oCompany = new Company();

            try
            {
                oCompany = Connection.GetCompany();
                return ret = BPGroup.EditBPGroup(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/DeleteBPGroup")]
        public MessageInfo DeleteBPGroup([FromBody] BPGroupModel model)
        {
            string funcname = "DeleteBPGroup";
            MessageInfo ret = null;
            Company oCompany = new Company();

            try
            {
                oCompany = Connection.GetCompany();
                return ret = BPGroup.DeleteBPGroup(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpGet]
        [Route("api/GetBPGroup")]
        public IHttpActionResult GetBPGroup(string GroupType)
        {
            string funcname = "GetBPGroup";
            MessageInfo ret = null;
            Company oCompany = new Company();

            try
            {
                oCompany = Connection.GetCompany();
                return Content(HttpStatusCode.OK, BPGroup.GetBPGroup(oCompany, GroupType));
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.OK, new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" });
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/GetBPGroupByID")]
        public object GetBPGroupByID([FromBody] BPGroupModel model)
        {
            string funcname = "GetBPGroupByID";
            Company oCompany = new Company();
            object ret = null;

            try
            {
                oCompany = Connection.GetCompany();
                return ret = BPGroup.GetBPGroupByID(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }
    }
}