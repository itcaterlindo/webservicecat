﻿using SAPbobsCOM;
using System;
using System.Web.Http;
using WebServiceCAT.Models;
using WebServiceCAT.SAP;

namespace WebServiceCAT.Controllers
{
    public class SalesOrderController : ApiController
    {
        [HttpPost]
        [Route("api/AddSalesOrder")]
        public MessageInfo AddSalesOrder([FromBody] SalesOrderModel model)
        {
            string funcname = "AddSalesOrder";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = SalesOrder.AddSalesOrder(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/EditSalesOrder")]
        public MessageInfo EditSalesOrder([FromBody] SalesOrderModel model)
        {
            string funcname = "EditSalesOrder";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = SalesOrder.EditSalesOrder(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/CloseSalesOrder")]
        public MessageInfo CloseSalesOrder([FromBody] SalesOrderModel model)
        {
            string funcname = "CloseSalesOrder";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = SalesOrder.CloseSalesOrder(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/CancelSalesOrder")]
        public MessageInfo CancelSalesOrder([FromBody] SalesOrderModel model)
        {
            string funcname = "CancelSalesOrder";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = SalesOrder.CancelSalesOrder(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/EditTglKirimSalesOrder")]
        public MessageInfo EditTglKirimSalesOrder([FromBody] SalesOrderModel model)
        {
            string funcname = "EditTglKirimSalesOrder";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = SalesOrder.EditTglKirimSalesOrder(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

    }
}