﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebServiceCAT.Models;
using WebServiceCAT.SAP;

namespace WebServiceCAT.Controllers
{
    public class ARDownPaymentController : ApiController
    {
        [HttpPost]
        [Route("api/AddARDP")]
        public MessageInfo AddARDP([FromBody] ARDownPaymentModel model)
        {
            string funcname = "AddARDP";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = ARDownPayment.AddARDP(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }
    }
}