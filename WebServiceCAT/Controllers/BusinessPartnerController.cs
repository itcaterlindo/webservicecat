﻿using SAPbobsCOM;
using System;
using System.Net;
using System.Web.Http;
using WebServiceCAT.Models;
using WebServiceCAT.SAP;

namespace WebServiceCAT.Controllers
{
    public class BusinessPartnerController : ApiController
    {
        [HttpPost]
        [Route("api/AddBP")]
        public MessageInfo AddBP([FromBody] BusinessPartnerModel model)
        {
            string funcname = "AddBP";
            MessageInfo ret = null;
            Company oCompany = new Company();

            try
            {
                oCompany = Connection.GetCompany();
                return ret = BusinessPartner.AddBP(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/EditBP")]
        public MessageInfo EditBP([FromBody] BusinessPartnerModel model)
        {
            string funcname = "EditBP";
            MessageInfo ret = null;
            Company oCompany = new Company();

            try
            {
                oCompany = Connection.GetCompany();
                return ret = BusinessPartner.EditBP(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpGet]
        [Route("api/GetBP")]
        public IHttpActionResult GetBP(string CardType)
        {
            string funcname = "GetBP";
            MessageInfo ret = null;
            Company oCompany = new Company();

            try
            {
                oCompany = Connection.GetCompany();
                return Content(HttpStatusCode.OK, BusinessPartner.GetBP(oCompany, CardType));
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.OK, new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" });
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/GetBPByID")]
        public object GetBPByID([FromBody] BusinessPartnerModel model)
        {
            string funcname = "GetBPByID";
            object ret = null;
            Company oCompany = new Company();

            try
            {
                oCompany = Connection.GetCompany();
                return ret = BusinessPartner.GetBPByID(oCompany, model.CardCode);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }
    }
}