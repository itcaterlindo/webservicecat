﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebServiceCAT.Models;
using WebServiceCAT.SAP;

namespace WebServiceCAT.Controllers
{
    public class TestingController : ApiController
    {
        [HttpPost]
        [Route("api/TestJSON")]
        public object TestJSON(Object bodyValue)
        {
            object ret = null;
            Company oCompany = new Company();
            try
            {
                return ret = bodyValue;
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = "Failed " + ex.Message.ToString() };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

    }
}