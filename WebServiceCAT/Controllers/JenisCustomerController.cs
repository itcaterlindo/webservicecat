﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using WebServiceCAT.Models;
using WebServiceCAT.SAP;

namespace WebServiceCAT.Controllers
{
    public class JenisCustomerController : ApiController
    {
        [HttpPost]
        [Route("api/AddJenisCustomer")]
        public MessageInfo AddJenisCustomer([FromBody] JenisCustomerModel model)
        {
            string funcname = "AddJenisCustomer";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = JenisCustomer.AddJenisCustomer(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/EditJenisCustomer")]
        public MessageInfo EditJenisCustomer([FromBody] JenisCustomerModel model)
        {
            string funcname = "EditJenisCustomer";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = JenisCustomer.EditJenisCustomer(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpGet]
        [Route("api/GetJenisCustomer")]
        public IHttpActionResult GetJenisCustomer()
        {
            string funcname = "GetJenisCustomer";
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return Content(HttpStatusCode.OK, JenisCustomer.GetJenisCustomer(oCompany));
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.OK, new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" });
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }
    }
}