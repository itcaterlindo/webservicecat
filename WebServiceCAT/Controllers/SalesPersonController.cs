﻿using SAPbobsCOM;
using System;
using System.Net;
using System.Web.Http;
using WebServiceCAT.Models;
using WebServiceCAT.SAP;

namespace WebServiceCAT.Controllers
{
    public class SalesPersonController : ApiController
    {
        [HttpPost]
        [Route("api/AddSalesPerson")]
        public MessageInfo AddSalesPerson([FromBody] SalesPersonModel model)
        {
            string funcname = "AddSalesPerson";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = SalesPerson.AddSalesPerson(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/EditSalesPerson")]
        public MessageInfo EditSalesPerson([FromBody] SalesPersonModel model)
        {
            string funcname = "EditSalesPerson";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = SalesPerson.EditSalesPerson(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpGet]
        [Route("api/GetSalesPerson")]
        public IHttpActionResult GetSalesPerson()
        {
            string funcname = "GetSalesPerson";
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return Content(HttpStatusCode.OK, SalesPerson.GetSalesPerson(oCompany));
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.OK, new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" });
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }
    }
}