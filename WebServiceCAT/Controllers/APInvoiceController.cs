﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebServiceCAT.Models;
using WebServiceCAT.SAP;

namespace WebServiceCAT.Controllers
{
    public class APInvoiceController : ApiController
    {
        [HttpPost]
        [Route("api/AddAPInvoice")]
        public MessageInfo AddAPInvoice([FromBody] APInvoiceModel model)
        {
            string funcname = "AddAPInvoice";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = APInvoice.AddAPInvoice(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }
    }
}