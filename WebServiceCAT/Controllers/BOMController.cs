﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebServiceCAT.Models;
using WebServiceCAT.SAP;

namespace WebServiceCAT.Controllers
{
    public class BOMController : ApiController
    {
        // POST: BOM
        [HttpPost]
        [Route("api/AddBOM")]
        public object AddBOM([FromBody] BOM_Header model)
        {
            object ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = BOM.AddBOM(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = "Failed - [AddBOM] " + ex.Message.ToString() };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        // POST: BOM
        [HttpPost]
        [Route("api/EditBOM")]
        public object EditBOM([FromBody] BOM_Header model)
        {
            object ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = BOM.EditBOM(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = "Failed - [EditBOM] " + ex.Message.ToString() };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }


    }
}