﻿using SAPbobsCOM;
using System;
using System.Net;
using System.Web.Http;
using WebServiceCAT.Models;
using WebServiceCAT.SAP;

namespace WebServiceCAT.Controllers
{
    public class ItemController : ApiController
    {
        [HttpPost]
        [Route("api/AddItem")]
        public MessageInfo AddItem([FromBody] ItemModel model)
        {
            string funcname = "AddItem";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = Item.AddItem(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/EditItem")]
        public MessageInfo EditItem([FromBody] ItemModel model)
        {
            string funcname = "EditItem";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = Item.EditItem(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpGet]
        [Route("api/GetItem")]
        public IHttpActionResult GetItem()
        {
            string funcname = "GetItem";
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return Content(HttpStatusCode.OK, Item.GetItem(oCompany));
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.OK, new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" });
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/GetItemByID")]
        public object GetItemByID([FromBody] ItemModel model)
        {
            string funcname = "GetItemByID";
            object ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = Item.GetItemByID(oCompany, model.ItemCode);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }
    }
}