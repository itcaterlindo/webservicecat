﻿using SAPbobsCOM;
using System;
using System.Web.Http;
using WebServiceCAT.Models;
using WebServiceCAT.SAP;

namespace WebServiceCAT.Controllers
{
    public class PurchaseOrderController : ApiController
    {
        [HttpPost]
        [Route("api/AddPurchaseOrder")]
        public MessageInfo AddPurchaseOrder([FromBody] PurchaseOrderModel model)
        {
            string funcname = "AddPurchaseOrder";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = PurchaseOrder.AddPurchaseOrder(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/EditPurchaseOrder")]
        public MessageInfo EditPurchaseOrder([FromBody] PurchaseOrderModel model)
        {
            string funcname = "EditPurchaseOrder";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = PurchaseOrder.EditPurchaseOrder(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/ClosePurchaseOrder")]
        public MessageInfo ClosePurchaseOrder([FromBody] PurchaseOrderModel model)
        {
            string funcname = "ClosePurchaseOrder";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = PurchaseOrder.ClosePurchaseOrder(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/CancelPurchaseOrder")]
        public MessageInfo CancelPurchaseOrder([FromBody] PurchaseOrderModel model)
        {
            string funcname = "CancelPurchaseOrder";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = PurchaseOrder.CancelPurchaseOrder(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }
    }
}