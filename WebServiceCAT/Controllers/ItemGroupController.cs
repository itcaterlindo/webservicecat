﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using WebServiceCAT.Models;
using WebServiceCAT.SAP;

namespace WebServiceCAT.Controllers
{
    public class ItemGroupController : ApiController
    {
        [HttpPost]
        [Route("api/AddItemGroup")]
        public MessageInfo AddItemGroup([FromBody] ItemGroupModel model)
        {
            string funcname = "AddItemGroup";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = ItemGroup.AddItemGroup(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/EditItemGroup")]
        public MessageInfo EditItemGroup([FromBody] ItemGroupModel model)
        {
            string funcname = "EditItemGroup";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = ItemGroup.EditItemGroup(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/DeleteItemGroup")]
        public object DeleteItemGroup([FromBody] ItemGroupModel model)
        {
            string funcname = "DeleteItemGroup";
            object ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = ItemGroup.DeleteItemGroup(oCompany, model.U_IDU_WEBID);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpGet]
        [Route("api/GetItemGroup")]
        public IHttpActionResult GetItemGroup()
        {
            string funcname = "GetItemGroup";
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return Content(HttpStatusCode.OK, ItemGroup.GetItemGroup(oCompany));
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.OK, new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" });
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/GetItemGroupByID")]
        public object GetItemGroupByID([FromBody] ItemGroupModel model)
        {
            string funcname = "GetItemGroupByID";
            object ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = ItemGroup.GetItemGroupByID(oCompany, model.U_IDU_WEBID);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }
    }
}