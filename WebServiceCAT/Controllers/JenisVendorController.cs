﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using WebServiceCAT.Models;
using WebServiceCAT.SAP;

namespace WebServiceCAT.Controllers
{
    public class JenisVendorController : ApiController
    {
        [HttpPost]
        [Route("api/AddJenisVendor")]
        public MessageInfo AddJenisVendor([FromBody] JenisVendorModel model)
        {
            string funcname = "AddJenisVendor";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = JenisVendor.AddJenisVendor(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpPost]
        [Route("api/EditJenisVendor")]
        public MessageInfo EditJenisVendor([FromBody] JenisVendorModel model)
        {
            string funcname = "EditJenisVendor";
            MessageInfo ret = null;
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return ret = JenisVendor.EditJenisVendor(oCompany, model);
            }
            catch (Exception ex)
            {
                return ret = new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }

        [HttpGet]
        [Route("api/GetJenisVendor")]
        public IHttpActionResult GetJenisVendor()
        {
            string funcname = "GetJenisVendor";
            Company oCompany = new Company();
            try
            {
                oCompany = Connection.GetCompany();
                return Content(HttpStatusCode.OK, JenisVendor.GetJenisVendor(oCompany));
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.OK, new MessageInfo() { ErrorCode = 1, Message = $"Failed - [{funcname}] {ex.ToString()}" });
            }
            finally
            {
                if (oCompany.Connected)
                {
                    oCompany.Disconnect();
                }
            }
        }
    }
}