﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using WebServiceCAT.Models;

namespace WebServiceCAT.SAP
{
    public class JenisVendor
    {
        public static MessageInfo AddJenisVendor(SAPbobsCOM.Company oCompany, JenisVendorModel model)
        {
            string funcname = "AddJenisVendor";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            UserTable udt = null;

            try
            {
                #region Cek Kode Sama
                Qry = $"SELECT \"Code\" FROM \"@JENIS_VENDOR\" WHERE UPPER(\"Code\") = UPPER('{model.Code}')";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count > 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data jenis vendor dengan kode '{model.Code}' sudah ada" };
                }
                #endregion

                #region Cek GroupNum
                Qry = $"SELECT \"GroupNum\" FROM OCTG WHERE \"GroupNum\" = '{model.U_IDU_GroupNum}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data payment terms dengan kode '{model.U_IDU_GroupNum}' tidak ada" };
                }
                #endregion

                #region Cek VatGroup
                Qry = $"SELECT \"Code\" FROM OVTG WHERE \"Code\" = '{model.U_IDU_VatGroup}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data tax dengan kode '{model.U_IDU_VatGroup}' tidak ada" };
                }
                #endregion

                oCompany.StartTransaction();
                udt = oCompany.UserTables.Item("JENIS_VENDOR");
                udt.Code = model.Code;
                udt.Name = model.Name;
                udt.UserFields.Fields.Item("U_IDU_Tipe").Value = model.U_IDU_Tipe;
                udt.UserFields.Fields.Item("U_IDU_GroupNum").Value = model.U_IDU_GroupNum;
                udt.UserFields.Fields.Item("U_IDU_VatGroup").Value = model.U_IDU_VatGroup;

                if (udt.Add() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {model.Code}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (udt != null)
                {
                    Marshal.ReleaseComObject(udt);
                    udt = null;
                }
            }
        }

        public static MessageInfo EditJenisVendor(SAPbobsCOM.Company oCompany, JenisVendorModel model)
        {
            string funcname = "EditJenisVendor";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            UserTable udt = null;

            try
            {
                #region Cek GroupNum
                Qry = $"SELECT \"GroupNum\" FROM OCTG WHERE \"GroupNum\" = '{model.U_IDU_GroupNum}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data payment terms dengan kode '{model.U_IDU_GroupNum}' tidak ada" };
                }
                #endregion

                #region Cek VatGroup
                Qry = $"SELECT \"Code\" FROM OVTG WHERE \"Code\" = '{model.U_IDU_VatGroup}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data tax dengan kode '{model.U_IDU_VatGroup}' tidak ada" };
                }
                #endregion

                oCompany.StartTransaction();
                udt = oCompany.UserTables.Item("JENIS_VENDOR");
                if (!udt.GetByKey(model.Code))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                udt.Name = model.Name;
                udt.UserFields.Fields.Item("U_IDU_Tipe").Value = model.U_IDU_Tipe;
                udt.UserFields.Fields.Item("U_IDU_GroupNum").Value = model.U_IDU_GroupNum;
                udt.UserFields.Fields.Item("U_IDU_VatGroup").Value = model.U_IDU_VatGroup;

                if (udt.Update() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {model.Code.Trim()}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (udt != null)
                {
                    Marshal.ReleaseComObject(udt);
                    udt = null;
                }
            }
        }

        public static object GetJenisVendor(SAPbobsCOM.Company oCompany)
        {
            string funcname = "GetJenisVendor";
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            try
            {
                Qry = $"SELECT \"Code\", \"Name\", \"U_IDU_Tipe\", \"U_IDU_GroupNum\", \"U_IDU_VatGroup\" FROM \"@JENIS_VENDOR\" ORDER BY \"Code\"";
                Function.OpenTable(oCompany, Qry, ref dt, ref tmp);
                if (tmp != "")
                    return new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };

                return dt;
            }
            catch (Exception ex)
            {
                return new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
        }
    }
}