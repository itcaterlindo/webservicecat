﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using WebServiceCAT.Models;

namespace WebServiceCAT.SAP
{
    public class APInvoice
    {
        public static MessageInfo AddAPInvoice(SAPbobsCOM.Company oCompany, APInvoiceModel model)
        {
            string funcname = "AddAPInvoice";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            Documents oAP = null;

            try
            {
                oCompany.StartTransaction();
                oAP = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oPurchaseInvoices);
                oAP.UserFields.Fields.Item("U_IDU_WEBID").Value = model.U_IDU_WEBID;
                oAP.UserFields.Fields.Item("U_IDU_AP_INTNUM").Value = model.U_IDU_AP_INTNUM;
                oAP.UserFields.Fields.Item("U_FP").Value = model.U_FP;
                oAP.DocDate = model.DocDate;
                oAP.DocDueDate = model.DocDueDate;
                oAP.NumAtCard = model.NumAtCard;
                oAP.DiscountPercent = model.DiscPrcnt;
                oAP.Comments = model.Comments;

                if (!string.IsNullOrWhiteSpace(model.WTCode))
                {
                    string[] wtcode = model.WTCode.Split(';');
                    for (int i = 0; i < wtcode.Count();i++)
                    {
                        oAP.WithholdingTaxData.WTCode = wtcode[i];
                        oAP.WithholdingTaxData.Add();
                    }
                }

                oAP.UserFields.Fields.Item("U_IDU_WEBUSER").Value = model.U_IDU_WEBUSER;

                Qry = $"SELECT T0.\"CardCode\", T0.\"GroupNum\" FROM OCRD T0 WHERE T0.\"CardCode\" = '{model.CardCode}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data vendor dengan CardCode '{model.CardCode}' tidak ada" };
                }
                string cardcode = dt.Rows[0]["CardCode"].ToString();
                string groupnum = dt.Rows[0]["GroupNum"].ToString();

                oAP.CardCode = cardcode;
                if (!string.IsNullOrWhiteSpace(groupnum))
                    oAP.GroupNumber = Convert.ToInt32(groupnum);

                for (int i = 0; i < model.Lines_Detail_Item.Count; i++)
                {
                    #region Cek Base
                    Qry = $"SELECT T1.\"DocEntry\", T1.\"ObjType\", T1.\"LineNum\" FROM OPDN T0 INNER JOIN PDN1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" WHERE T0.\"CANCELED\" = 'N' AND T1.\"LineStatus\" = 'O' AND T1.\"U_IDU_WEBID\" = '{model.Lines_Detail_Item[i].KdGRPODtl}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data goods receipt po detail dengan kode web '{model.Lines_Detail_Item[i].KdGRPODtl}' tidak ada" };
                    }
                    #endregion

                    string docentrypo = dt.Rows[0]["DocEntry"].ToString();
                    oAP.Lines.BaseEntry = Convert.ToInt32(dt.Rows[0]["DocEntry"].ToString());
                    oAP.Lines.BaseType = Convert.ToInt32(dt.Rows[0]["ObjType"].ToString());
                    oAP.Lines.BaseLine = Convert.ToInt32(dt.Rows[0]["LineNum"].ToString());
                    oAP.Lines.Quantity = model.Lines_Detail_Item[i].Quantity * model.Lines_Detail_Item[i].Konversi;
                    oAP.Lines.LineTotal = model.Lines_Detail_Item[i].LineTotal;
                    oAP.Lines.VatGroup = model.VatGroup;

                    if (model.Lines_Detail_Item[i].WithholdingTax.ToLower() == "y")
                        oAP.Lines.WTLiable = BoYesNoEnum.tYES;
                    else if (model.Lines_Detail_Item[i].WithholdingTax.ToLower() == "n")
                        oAP.Lines.WTLiable = BoYesNoEnum.tNO;

                    oAP.Lines.UserFields.Fields.Item("U_IDU_WEBID").Value = model.Lines_Detail_Item[i].U_IDU_WEBID;
                    oAP.Lines.Add();

                    Qry = $"SELECT \"DocEntry\" FROM ODPO A WHERE A.\"CANCELED\" = 'N' AND A.\"PaidSum\" > 0 AND A.\"DocEntry\" IN (SELECT \"DocEntry\" FROM DPO1 B WHERE B.\"BaseType\" = '17' AND B.\"BaseEntry\" = '{docentrypo}')";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }

                    //Tarik Down Payment SO
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        SAPbobsCOM.Documents oDPM = (Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDownPayments);
                        if (!oDPM.GetByKey(Convert.ToInt32(dt.Rows[j]["DocEntry"])))
                        {
                            Error = true;
                            return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Ambil data DP gagal" };
                        }
                        DownPaymentsToDraw dptodraw = oAP.DownPaymentsToDraw;
                        dptodraw.DocEntry = oDPM.DocEntry;
                        dptodraw.AmountToDraw = oDPM.DownPaymentAmount;
                        dptodraw.Add();
                    }
                }

                if (oAP.Add() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }

                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {oCompany.GetNewObjectKey()}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oAP != null)
                {
                    Marshal.ReleaseComObject(oAP);
                    oAP = null;
                }
            }
        }
    }
}