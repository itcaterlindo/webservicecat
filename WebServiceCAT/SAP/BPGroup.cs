﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Web;
using WebServiceCAT.Models;

namespace WebServiceCAT.SAP
{
    public class BPGroup
    {
        public static MessageInfo AddBPGroup(SAPbobsCOM.Company oCompany, BPGroupModel model)
        {
            string funcname = "AddBPGroup";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            BusinessPartnerGroups oGrp = null;

            try
            {
                #region Cek Kode Sama
                Qry = $"SELECT \"GroupCode\" FROM OCRG WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count > 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data customer/vendor group dengan kode web '{model.U_IDU_WEBID}' sudah ada" };
                }
                #endregion

                oCompany.StartTransaction();
                oGrp = (BusinessPartnerGroups)oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartnerGroups);
                oGrp.Name = model.GroupName;
                if (model.GroupType.ToLower() == "c")
                    oGrp.Type = BoBusinessPartnerGroupTypes.bbpgt_CustomerGroup;
                else if (model.GroupType.ToLower() == "s")
                    oGrp.Type = BoBusinessPartnerGroupTypes.bbpgt_VendorGroup;
                oGrp.UserFields.Fields.Item("U_IDU_WEBID").Value = model.U_IDU_WEBID;

                if (oGrp.Add() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {model.U_IDU_WEBID}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oGrp != null)
                {
                    Marshal.ReleaseComObject(oGrp);
                    oGrp = null;
                }
            }
        }

        public static MessageInfo EditBPGroup(SAPbobsCOM.Company oCompany, BPGroupModel model)
        {
            string funcname = "EditBPGroup";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            BusinessPartnerGroups oGrp = null;

            try
            {
                #region Ambil GroupCode
                Qry = $"SELECT \"GroupCode\" FROM OCRG WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data customer group dengan kode web '{model.U_IDU_WEBID}' tidak ada" };
                }
                string groupcode = dt.Rows[0][0].ToString();
                #endregion

                oCompany.StartTransaction();
                oGrp = (BusinessPartnerGroups)oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartnerGroups);
                if (!oGrp.GetByKey(Convert.ToInt32(groupcode)))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                oGrp.Name = model.GroupName;

                if (oGrp.Update() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {model.U_IDU_WEBID}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oGrp != null)
                {
                    Marshal.ReleaseComObject(oGrp);
                    oGrp = null;
                }
            }
        }

        public static MessageInfo DeleteBPGroup(SAPbobsCOM.Company oCompany, BPGroupModel model)
        {
            string funcname = "DeleteBPGroup";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            BusinessPartnerGroups oGrp = null;

            try
            {
                #region Ambil GroupCode
                Qry = $"SELECT \"GroupCode\" FROM OCRG WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data customer group dengan kode web '{model.U_IDU_WEBID}' tidak ada" };
                }
                string groupcode = dt.Rows[0][0].ToString();
                #endregion

                oCompany.StartTransaction();
                oGrp = (BusinessPartnerGroups)oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartnerGroups);
                if (!oGrp.GetByKey(Convert.ToInt32(groupcode)))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }

                if (oGrp.Remove() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {model.U_IDU_WEBID}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oGrp != null)
                {
                    Marshal.ReleaseComObject(oGrp);
                    oGrp = null;
                }
            }
        }

        public static object GetBPGroup(SAPbobsCOM.Company oCompany, string GroupType)
        {
            string funcname = "GetBPGroup";
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();

            try
            {
                Qry = $"SELECT \"U_IDU_WEBID\", \"GroupName\", \"GroupType\" FROM OCRG WHERE COALESCE(\"U_IDU_WEBID\",'') <> '' AND \"GroupType\" = '{GroupType}' ORDER BY \"GroupName\"";
                Function.OpenTable(oCompany, Qry, ref dt, ref tmp);
                if (tmp != "")
                    return new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };

                return dt;
            }
            catch (Exception ex)
            {
                return new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
        }

        public static object GetBPGroupByID(SAPbobsCOM.Company oCompany, BPGroupModel model)
        {
            string funcname = "GetBPGroupByID";
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();

            try
            {
                Qry = $"SELECT \"U_IDU_WEBID\", \"GroupName\", \"GroupType\" FROM OCRG WHERE COALESCE(\"U_IDU_WEBID\",'') <> '' AND \"U_IDU_WEBID\" = '{model.U_IDU_WEBID}' ORDER BY \"GroupName\"";
                Function.OpenTable(oCompany, Qry, ref dt, ref tmp);
                if (tmp != "")
                    return new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };

                return dt;
            }
            catch (Exception ex)
            {
                return new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
        }
    }
}