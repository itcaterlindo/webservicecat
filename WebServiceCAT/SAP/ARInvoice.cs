﻿using Microsoft.Win32.SafeHandles;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using WebServiceCAT.Models;

namespace WebServiceCAT.SAP
{
    public class ARInvoice
    {
        public static MessageInfo AddARInvoice(SAPbobsCOM.Company oCompany, ARInvoiceModel model)
        {
            string funcname = "AddARInvoice";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            Documents oAR = null;
            Documents oSO = null;

            try
            {
                #region Ambil DocEntry
                Qry = $"SELECT \"DocEntry\", \"U_IDU_SO_INTNUM\" FROM ORDR WHERE \"U_IDU_WEBID\" = '{model.KdSalesOrder}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data sales order dengan kode '{model.KdSalesOrder}' tidak ada" };
                }
                string docentry = dt.Rows[0]["DocEntry"].ToString();
                string sointnum = dt.Rows[0]["U_IDU_SO_INTNUM"].ToString();
                #endregion

                string satu = "abc";
                string dua = "def";
                //string tiga = satu + dua;

                oCompany.StartTransaction();
                oSO = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oOrders);
                if (!oSO.GetByKey(Convert.ToInt32(docentry)))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }

                oAR = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oInvoices);
                oAR.UserFields.Fields.Item("U_IDU_WEBID").Value = model.U_IDU_WEBID;
                oAR.UserFields.Fields.Item("U_IDU_SO_INTNUM").Value = sointnum;

                oAR.SalesPersonCode = oSO.SalesPersonCode;

                oAR.UserFields.Fields.Item("U_IDU_AR_INTNUM").Value = model.U_IDU_AR_INTNUM;
                oAR.DocDate = model.DocDate;
                oAR.DocDueDate = model.DocDate;
                oAR.NumAtCard = model.NumAtCard;
                oAR.Comments = model.Comments;
                oAR.UserFields.Fields.Item("U_IDU_Stuffing_Date").Value = model.U_IDU_Stuffing_Date;
                oAR.Address2 = oSO.Address2;

                Qry = $"SELECT \"ExpnsCode\", \"LineTotal\" FROM RDR3 WHERE \"DocEntry\" = '{docentry}'ORDER BY \"ExpnsCode\"";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        oAR.Expenses.ExpenseCode = Convert.ToInt32(dt.Rows[i]["ExpnsCode"]);
                        oAR.Expenses.LineTotal = Convert.ToDouble(dt.Rows[i]["LineTotal"]);
                        oAR.Expenses.Add();
                    }
                }

                oAR.UserFields.Fields.Item("U_IDU_WEBUSER").Value = model.U_IDU_WEBUSER;

                Qry = $"SELECT T0.\"CardCode\", T1.\"U_IDU_GroupNum\", T1.\"U_IDU_VatGroup\" FROM OCRD T0 LEFT JOIN \"@JENIS_CUSTOMER\" T1 ON T0.\"U_IDU_KodeJenisCustomer\" = T1.\"Code\" WHERE T0.\"CardCode\" = '{oSO.CardCode}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data jenis customer dengan CardCode '{oSO.CardCode}' tidak ada" };
                }
                string cardcode = dt.Rows[0]["CardCode"].ToString();
                string groupnum = dt.Rows[0]["U_IDU_GroupNum"].ToString();
                string vatgroup = dt.Rows[0]["U_IDU_VatGroup"].ToString();

                oAR.CardCode = cardcode;
                if (!string.IsNullOrWhiteSpace(groupnum))
                    oAR.GroupNumber = Convert.ToInt32(groupnum);


                for (int i = 0; i < model.Lines_Detail_Item.Count; i++)
                {
                    #region Cek Base
                    Qry = $"SELECT T1.\"DocEntry\", T1.\"ObjType\", T1.\"LineNum\" FROM ORDR T0 INNER JOIN RDR1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" WHERE T0.\"CANCELED\" = 'N' AND T1.\"LineStatus\" = 'O' AND T1.\"U_IDU_WEBID\" = '{model.Lines_Detail_Item[i].KdSalesOrderDtl}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data sales order detail dengan kode web '{model.Lines_Detail_Item[i].KdSalesOrderDtl}' tidak ada" };
                    }
                    #endregion

                    string docentryso = dt.Rows[0]["DocEntry"].ToString();
                    oAR.Lines.BaseEntry = Convert.ToInt32(dt.Rows[0]["DocEntry"].ToString());
                    oAR.Lines.BaseType = Convert.ToInt32(dt.Rows[0]["ObjType"].ToString());
                    oAR.Lines.BaseLine = Convert.ToInt32(dt.Rows[0]["LineNum"].ToString());
                    oAR.Lines.Quantity = model.Lines_Detail_Item[i].Quantity;

                    #region Cek Warehouse
                    Qry = $"SELECT \"WhsCode\" FROM OWHS WHERE \"U_IDU_WEBID\" = '{model.Lines_Detail_Item[i].KdWarehouse}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data warehouse dengan kode web '{model.Lines_Detail_Item[i].KdWarehouse}' tidak ada" };
                    }
                    string whscode = dt.Rows[0][0].ToString();
                    #endregion

                    oAR.Lines.WarehouseCode = whscode;

                    oAR.Lines.UserFields.Fields.Item("U_IDU_WEBID").Value = model.Lines_Detail_Item[i].U_IDU_WEBID;
                    oAR.Lines.Add();

                    Qry = $"SELECT \"DocEntry\" FROM ODPI A WHERE A.\"CANCELED\" = 'N' AND A.\"PaidSum\" > 0 AND A.\"DocEntry\" IN (SELECT \"DocEntry\" FROM DPI1 B WHERE B.\"BaseType\" = '17' AND B.\"BaseEntry\" = '{docentryso}')";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }

                    //Tarik Down Payment SO
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        SAPbobsCOM.Documents oDPM = (Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDownPayments);
                        if (!oDPM.GetByKey(Convert.ToInt32(dt.Rows[j]["DocEntry"])))
                        {
                            Error = true;
                            return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Ambil data DP gagal" };
                        }
                        DownPaymentsToDraw dptodraw = oAR.DownPaymentsToDraw;
                        dptodraw.DocEntry = oDPM.DocEntry;
                        dptodraw.AmountToDraw = oDPM.DownPaymentAmount;
                        dptodraw.Add();
                    }
                }

                if (oAR.Add() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }

                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {oCompany.GetNewObjectKey()}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oAR != null)
                {
                    Marshal.ReleaseComObject(oAR);
                    oAR = null;
                }
            }
        }
    }
}