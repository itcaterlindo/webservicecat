﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using WebServiceCAT.Models;

namespace WebServiceCAT.SAP
{
    public class GoodsReceiptPO
    {
        public static MessageInfo AddGRPO(SAPbobsCOM.Company oCompany, GoodsReceiptPOModel model)
        {
            string funcname = "AddGRPO";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            Documents oGRPO = null;

            try
            {
                #region Ambil DocEntry
                Qry = $"SELECT T0.\"DocEntry\", T0.\"U_IDU_PO_INTNUM\", T0.\"CardCode\" FROM OPOR T0 INNER JOIN POR1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" WHERE T1.\"U_IDU_WEBID\" = '{model.Lines_Detail_Item[0].KdPurchaseOrderDtl}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data purchase order dengan kode detail '{model.Lines_Detail_Item[0].KdPurchaseOrderDtl}' tidak ada" };
                }
                string cardcode = dt.Rows[0]["CardCode"].ToString();
                #endregion

                oCompany.StartTransaction();
                oGRPO = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oPurchaseDeliveryNotes);
                oGRPO.CardCode = cardcode;
                oGRPO.UserFields.Fields.Item("U_IDU_WEBID").Value = model.U_IDU_WEBID;
                oGRPO.DocDate = model.DocDate;
                oGRPO.DocDueDate = model.DocDate;
                oGRPO.NumAtCard = model.NumAtCard;
                oGRPO.Comments = model.Comments;
                oGRPO.UserFields.Fields.Item("U_IDU_NoSJ").Value = model.U_IDU_NoSJ;
                oGRPO.UserFields.Fields.Item("U_IDU_TglSJ").Value = model.U_IDU_TglSJ;
                oGRPO.UserFields.Fields.Item("U_IDU_WEBUSER").Value = model.U_IDU_WEBUSER;

                for (int i = 0; i < model.Lines_Detail_Item.Count; i++)
                {
                    #region Cek Base
                    Qry = $"SELECT T1.\"DocEntry\", T1.\"ObjType\", T1.\"LineNum\" FROM OPOR T0 INNER JOIN POR1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" WHERE T0.\"CANCELED\" = 'N' AND T1.\"LineStatus\" = 'O' AND T1.\"U_IDU_WEBID\" = '{model.Lines_Detail_Item[i].KdPurchaseOrderDtl}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data purchase order detail dengan kode web '{model.Lines_Detail_Item[i].KdPurchaseOrderDtl}' tidak ada" };
                    }
                    #endregion

                    oGRPO.Lines.BaseEntry = Convert.ToInt32(dt.Rows[0]["DocEntry"].ToString());
                    oGRPO.Lines.BaseType = Convert.ToInt32(dt.Rows[0]["ObjType"].ToString());
                    oGRPO.Lines.BaseLine = Convert.ToInt32(dt.Rows[0]["LineNum"].ToString());
                    oGRPO.Lines.Quantity = model.Lines_Detail_Item[i].Quantity * model.Lines_Detail_Item[i].Konversi;
                    oGRPO.Lines.UserFields.Fields.Item("U_IDU_WEBID").Value = model.Lines_Detail_Item[i].U_IDU_WEBID;

                    #region Cek Warehouse
                    Qry = $"SELECT \"WhsCode\" FROM OWHS WHERE \"U_IDU_WEBID\" = '{model.Lines_Detail_Item[i].KdWarehouse}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data warehouse dengan kode web '{model.Lines_Detail_Item[i].KdWarehouse}' tidak ada" };
                    }
                    string whscode = dt.Rows[0][0].ToString();
                    #endregion

                    oGRPO.Lines.WarehouseCode = whscode;
                    oGRPO.Lines.Add();
                }

                if (oGRPO.Add() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }

                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {oCompany.GetNewObjectKey()}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oGRPO != null)
                {
                    Marshal.ReleaseComObject(oGRPO);
                    oGRPO = null;
                }
            }
        }
    }
}