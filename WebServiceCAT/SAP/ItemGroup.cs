﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using WebServiceCAT.Models;

namespace WebServiceCAT.SAP
{
    public class ItemGroup
    {
        public static MessageInfo AddItemGroup(SAPbobsCOM.Company oCompany, ItemGroupModel model)
        {
            string funcname = "AddItemGroup";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            ItemGroups oGrp = null;

            try
            {
                #region Cek Kode Sama
                Qry = $"SELECT \"ItmsGrpCod\" FROM OITB WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count > 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data item group dengan kode web '{model.U_IDU_WEBID}' sudah ada" };
                }
                #endregion

                oCompany.StartTransaction();
                oGrp = (ItemGroups)oCompany.GetBusinessObject(BoObjectTypes.oItemGroups);
                oGrp.GroupName = model.ItmsGrpNam;
                oGrp.UserFields.Fields.Item("U_IDU_WEBID").Value = model.U_IDU_WEBID;

                if (oGrp.Add() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {model.U_IDU_WEBID}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oGrp != null)
                {
                    Marshal.ReleaseComObject(oGrp);
                    oGrp = null;
                }
            }
        }

        public static MessageInfo EditItemGroup(SAPbobsCOM.Company oCompany, ItemGroupModel model)
        {
            string funcname = "EditItemGroup";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            ItemGroups oGrp = null;

            try
            {
                #region Ambil ItmsGrpCod
                Qry = $"SELECT \"ItmsGrpCod\" FROM OITB WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data item group dengan kode web '{model.U_IDU_WEBID}' tidak ada" };
                }
                tmp = dt.Rows[0][0].ToString();
                #endregion

                oCompany.StartTransaction();
                oGrp = (ItemGroups)oCompany.GetBusinessObject(BoObjectTypes.oItemGroups);
                if (!oGrp.GetByKey(Convert.ToInt32(tmp)))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                oGrp.GroupName = model.ItmsGrpNam;

                if (oGrp.Update() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {model.U_IDU_WEBID}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oGrp != null)
                {
                    Marshal.ReleaseComObject(oGrp);
                    oGrp = null;
                }
            }
        }

        public static MessageInfo DeleteItemGroup(SAPbobsCOM.Company oCompany, string U_IDU_WEBID)
        {
            string funcname = "DeleteItemGroup";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            ItemGroups oGrp = null;

            try
            {
                #region Ambil ItmsGrpCod
                Qry = $"SELECT \"ItmsGrpCod\" FROM OITB WHERE \"U_IDU_WEBID\" = '{U_IDU_WEBID}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data item group dengan kode web '{U_IDU_WEBID}' tidak ada" };
                }
                tmp = dt.Rows[0][0].ToString();
                #endregion

                oCompany.StartTransaction();
                oGrp = (ItemGroups)oCompany.GetBusinessObject(BoObjectTypes.oItemGroups);
                if (!oGrp.GetByKey(Convert.ToInt32(tmp)))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }

                if (oGrp.Remove() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {U_IDU_WEBID}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oGrp != null)
                {
                    Marshal.ReleaseComObject(oGrp);
                    oGrp = null;
                }
            }
        }

        public static object GetItemGroup(SAPbobsCOM.Company oCompany)
        {
            string funcname = "GetItemGroup";
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            try
            {
                Qry = $"SELECT \"U_IDU_WEBID\", \"ItmsGrpNam\", \"U_IDU_WEBID\" FROM OITB WHERE COALESCE(\"U_IDU_WEBID\",'') <> '' ORDER BY \"ItmsGrpNam\"";
                Function.OpenTable(oCompany, Qry, ref dt, ref tmp);
                if (tmp != "")
                    return new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };

                return dt;
            }
            catch (Exception ex)
            {
                return new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
        }

        public static object GetItemGroupByID(SAPbobsCOM.Company oCompany, string U_IDU_WEBID)
        {
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            try
            {
                Qry = $"SELECT \"U_IDU_WEBID\", \"ItmsGrpNam\", \"U_IDU_WEBID\" FROM OITB WHERE \"U_IDU_WEBID\" = '{U_IDU_WEBID}'";
                Function.OpenTable(oCompany, Qry, ref dt, ref tmp);
                if (tmp != "")
                    return new MessageInfo() { ErrorCode = 2, Message = $"Failed - [GetItemGroupByID] {tmp}" };

                return dt;
            }
            catch (Exception ex)
            {
                return new MessageInfo() { ErrorCode = 99, Message = $"Failed - [GetItemGroupByID] {ex.ToString()}" };
            }
        }
    }
}