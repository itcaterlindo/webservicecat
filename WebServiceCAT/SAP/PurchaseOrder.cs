﻿using SAPbobsCOM;
using System;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using WebServiceCAT.Models;

namespace WebServiceCAT.SAP
{
    public class PurchaseOrder
    {
        public static MessageInfo AddPurchaseOrder(SAPbobsCOM.Company oCompany, PurchaseOrderModel model)
        {
            string funcname = "AddPurchaseOrder";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            Documents oPO = null;

            try
            {
                #region Cek Nomor Sama
                Qry = $"SELECT \"DocEntry\" FROM OPOR WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID.Trim()}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count > 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data purchase order dengan kode '{model.U_IDU_WEBID}' sudah ada" };
                }
                #endregion

                oCompany.StartTransaction();
                oPO = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oPurchaseOrders);
                oPO.UserFields.Fields.Item("U_IDU_WEBID").Value = model.U_IDU_WEBID;
                oPO.UserFields.Fields.Item("U_IDU_PO_INTNUM").Value = model.U_IDU_PO_INTNUM;
                oPO.UserFields.Fields.Item("U_IDU_PR_INTNUM").Value = model.U_IDU_PR_INTNUM;
                oPO.DocDate = model.DocDate;
                oPO.NumAtCard = model.NumAtCard;
                oPO.Comments = model.Comments;
                oPO.UserFields.Fields.Item("U_IDU_WEBUSER").Value = model.U_IDU_WEBUSER;

                Qry = $"SELECT T0.\"CardCode\", T0.\"GroupNum\", T0.\"ECVatGroup\" FROM OCRD T0 WHERE T0.\"CardCode\" = '{model.CardCode}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data jenis customer dengan CardCode '{model.CardCode}' tidak ada" };
                }
                string cardcode = dt.Rows[0]["CardCode"].ToString();
                string groupnum = dt.Rows[0]["GroupNum"].ToString();
                string vatgroup = dt.Rows[0]["ECVatGroup"].ToString();

                oPO.CardCode = cardcode;
                if (!string.IsNullOrWhiteSpace(groupnum))
                    oPO.GroupNumber = Convert.ToInt32(groupnum);

                for (int i = 0; i < model.Lines_Detail_Item.Count; i++)
                {
                    oPO.Lines.UserFields.Fields.Item("U_IDU_WEBID").Value = model.Lines_Detail_Item[i].U_IDU_WEBID;
                    oPO.Lines.ItemCode = model.Lines_Detail_Item[i].ItemCode;
                    oPO.Lines.ItemDescription = model.Lines_Detail_Item[i].Dscription;
                    oPO.Lines.UserFields.Fields.Item("U_IDU_Spesifikasi").Value = model.Lines_Detail_Item[i].U_IDU_Spesifikasi;

                    #region Cek Currency
                    Qry = $"SELECT \"CurrCode\" FROM OCRN WHERE \"U_IDU_WEBID\" = '{model.Lines_Detail_Item[i].KdCurrency}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data currency dengan kode web '{model.Lines_Detail_Item[i].KdCurrency}' tidak ada" };
                    }
                    string currcode = dt.Rows[0][0].ToString();
                    #endregion

                    oPO.Lines.Currency = currcode;
                    oPO.DocCurrency = currcode;
                    oPO.Lines.Quantity = model.Lines_Detail_Item[i].Quantity * model.Lines_Detail_Item[i].Konversi;
                    oPO.Lines.MeasureUnit = model.Lines_Detail_Item[i].UomName;

                    if (!string.IsNullOrWhiteSpace(vatgroup))
                        oPO.Lines.VatGroup = vatgroup;

                    oPO.Lines.LineTotal = model.Lines_Detail_Item[i].LineTotal;
                    oPO.Lines.ShipDate = model.Lines_Detail_Item[i].ShipDate;
                    oPO.Lines.Add();
                }

                if (oPO.Add() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                string docentryso = oCompany.GetNewObjectKey();

                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {docentryso}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oPO != null)
                {
                    Marshal.ReleaseComObject(oPO);
                    oPO = null;
                }
            }
        }

        public static MessageInfo EditPurchaseOrder(SAPbobsCOM.Company oCompany, PurchaseOrderModel model)
        {
            string funcname = "EditPurchaseOrder";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            Documents oPO = null;

            try
            {
                #region Cek PO Close
                Qry = $"SELECT \"DocEntry\" FROM OPOR WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID.Trim()}' AND \"DocStatus\" = 'C'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count > 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data purchase order dengan kode '{model.U_IDU_WEBID}' sudah closed" };
                }
                #endregion

                #region Ambil DocEntry
                Qry = $"SELECT \"DocEntry\" FROM OPOR WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID.Trim()}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data purchase order dengan kode '{model.U_IDU_WEBID}' tidak ada" };
                }
                string docentry = dt.Rows[0][0].ToString();
                #endregion

                oCompany.StartTransaction();
                oPO = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oPurchaseOrders);

                if (!oPO.GetByKey(Convert.ToInt32(docentry)))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }

                oPO.UserFields.Fields.Item("U_IDU_PO_INTNUM").Value = model.U_IDU_PO_INTNUM;
                oPO.UserFields.Fields.Item("U_IDU_PR_INTNUM").Value = model.U_IDU_PR_INTNUM;
                oPO.DocDate = model.DocDate;
                oPO.NumAtCard = model.NumAtCard;
                oPO.Comments = model.Comments;
                oPO.UserFields.Fields.Item("U_IDU_WEBUSER").Value = model.U_IDU_WEBUSER;

                Qry = $"SELECT T0.\"CardCode\", T0.\"GroupNum\", T0.\"ECVatGroup\" FROM OCRD T0 WHERE T0.\"CardCode\" = '{model.CardCode}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data jenis customer dengan CardCode '{model.CardCode}' tidak ada" };
                }
                string cardcode = dt.Rows[0]["CardCode"].ToString();
                string groupnum = dt.Rows[0]["GroupNum"].ToString();
                string vatgroup = dt.Rows[0]["ECVatGroup"].ToString();

                oPO.CardCode = cardcode;
                if (!string.IsNullOrWhiteSpace(groupnum))
                    oPO.GroupNumber = Convert.ToInt32(groupnum);

                for (int i = 0; i < model.Lines_Detail_Item.Count; i++)
                {
                    Qry = $"SELECT \"DocEntry\" FROM POR1 WHERE \"DocEntry\" = '{docentry}' AND \"U_IDU_WEBID\" = '{model.Lines_Detail_Item[i].U_IDU_WEBID}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count > 0)
                    {
                        continue;
                    }

                    oPO.Lines.Add();
                    oPO.Lines.UserFields.Fields.Item("U_IDU_WEBID").Value = model.Lines_Detail_Item[i].U_IDU_WEBID;
                    oPO.Lines.ItemCode = model.Lines_Detail_Item[i].ItemCode;
                    oPO.Lines.ItemDescription = model.Lines_Detail_Item[i].Dscription;
                    oPO.Lines.UserFields.Fields.Item("U_IDU_Spesifikasi").Value = model.Lines_Detail_Item[i].U_IDU_Spesifikasi;

                    #region Cek Currency
                    Qry = $"SELECT \"CurrCode\" FROM OCRN WHERE \"U_IDU_WEBID\" = '{model.Lines_Detail_Item[i].KdCurrency}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data currency dengan kode web '{model.U_IDU_WEBID}' tidak ada" };
                    }
                    string currcode = dt.Rows[0][0].ToString();
                    #endregion

                    oPO.Lines.Currency = currcode;
                    oPO.Lines.Quantity = model.Lines_Detail_Item[i].Quantity * model.Lines_Detail_Item[i].Konversi;
                    oPO.Lines.MeasureUnit = model.Lines_Detail_Item[i].UomName;

                    if (!string.IsNullOrWhiteSpace(vatgroup))
                        oPO.Lines.VatGroup = vatgroup;

                    oPO.Lines.LineTotal = model.Lines_Detail_Item[i].LineTotal;
                    oPO.Lines.ShipDate = model.Lines_Detail_Item[i].ShipDate;
                }

                if (oPO.Update() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                string docentryso = oCompany.GetNewObjectKey();
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {docentryso}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oPO != null)
                {
                    Marshal.ReleaseComObject(oPO);
                    oPO = null;
                }
            }
        }

        public static MessageInfo ClosePurchaseOrder(SAPbobsCOM.Company oCompany, PurchaseOrderModel model)
        {
            string funcname = "ClosePurchaseOrder";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            Documents oPO = null;

            try
            {
                #region Ambil DocEntry
                Qry = $"SELECT \"DocEntry\" FROM OPOR WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data purchase order dengan kode web '{model.U_IDU_WEBID}' tidak ada" };
                }
                string docentry = dt.Rows[0][0].ToString();
                #endregion

                #region Cek PO Closed
                Qry = $"SELECT \"DocEntry\" FROM OPOR WHERE \"DocEntry\" = '{docentry}' AND \"DocStatus\" = 'O'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data purchase order dengan kode web '{model.U_IDU_WEBID}' sudah closed" };
                }
                #endregion

                oCompany.StartTransaction();
                oPO = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oPurchaseOrders);
                if (!oPO.GetByKey(Convert.ToInt32(docentry)))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }

                if (oPO.Close() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {model.U_IDU_WEBID}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oPO != null)
                {
                    Marshal.ReleaseComObject(oPO);
                    oPO = null;
                }
            }
        }

        public static MessageInfo CancelPurchaseOrder(SAPbobsCOM.Company oCompany, PurchaseOrderModel model)
        {
            string funcname = "CancelPurchaseOrder";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            Documents oPO = null;

            try
            {
                #region Ambil DocEntry
                Qry = $"SELECT \"DocEntry\" FROM OPOR WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data purchase order dengan kode web '{model.U_IDU_WEBID}' tidak ada" };
                }
                string docentry = dt.Rows[0][0].ToString();
                #endregion

                oCompany.StartTransaction();
                oPO = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oOrders);
                if (!oPO.GetByKey(Convert.ToInt32(docentry)))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }

                if (oPO.Cancel() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {model.U_IDU_WEBID}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oPO != null)
                {
                    Marshal.ReleaseComObject(oPO);
                    oPO = null;
                }
            }
        }
    }
}