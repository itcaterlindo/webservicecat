﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace WebServiceCAT.SAP
{
    public class Connection
    {
        private static List<Company> AllCompany = new List<Company>();

        private static string DBType { get { return Properties.Settings.Default.DBType.Trim(); } }
        private static string DBServerName { get { return Properties.Settings.Default.DBServerName.Trim(); } }
        private static string DBUserName { get { return Properties.Settings.Default.DBUserName.Trim(); } }
        private static string DBPassword { get { return Encoding.UTF8.GetString(Convert.FromBase64String(Properties.Settings.Default.DBPassword.Trim())); } }
        private static string DBName { get { return Properties.Settings.Default.DBName.Trim(); } }
        private static string B1UserName { get { return Properties.Settings.Default.B1UserName.Trim(); } }
        private static string B1Password { get { return Encoding.UTF8.GetString(Convert.FromBase64String(Properties.Settings.Default.B1Password.Trim())); } }
        private static string B1License { get { return Properties.Settings.Default.B1License.Trim(); } }
        private static string B1SLD { get { return Properties.Settings.Default.B1SLD.Trim(); } }

        public static Company GetCompany()
        {
            try
            {
                foreach (Company com in AllCompany.ToList())
                {
                    if (!com.Connected)
                    {
                        AllCompany.Remove(com);
                    }
                }
                Company NewCom = NewCompany();
                if (!NewCom.Connected)
                {
                    throw new Exception("Connection error: " + NewCom.GetLastErrorCode() + " | " + NewCom.GetLastErrorDescription());
                }

                //Enable Approval
                AdminInfo oAdminInfo = NewCom.GetCompanyService().GetAdminInfo();
                oAdminInfo.EnableApprovalProcedureInDI = SAPbobsCOM.BoYesNoEnum.tYES;
                oAdminInfo.DocConfirmation = SAPbobsCOM.BoYesNoEnum.tYES;
                NewCom.GetCompanyService().UpdateAdminInfo(oAdminInfo);

                AllCompany.Add(NewCom);
                Function.CreateLog("");
                return NewCom;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static Company NewCompany()
        {
            try
            {
                Company NewCom = new Company();
                if (DBType.ToLower().Contains("hana"))
                {
                    NewCom.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                }
                else
                {
                    if (DBType.Contains("2005"))
                        NewCom.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2005;
                    else if (DBType.Contains("2008"))
                        NewCom.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
                    else if (DBType.Contains("2012"))
                        NewCom.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                    else if (DBType.Contains("2014"))
                        NewCom.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                    else if (DBType.Contains("2016"))
                        NewCom.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016;
                    else if (DBType.Contains("2017"))
                        NewCom.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2017;
                    else if (DBType.Contains("2019"))
                        NewCom.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2019;
                }
                NewCom.Server = DBServerName;
                NewCom.DbUserName = DBUserName;
                NewCom.DbPassword = DBPassword;
                NewCom.CompanyDB = DBName;
                NewCom.UserName = B1UserName;
                NewCom.Password = B1Password;
                NewCom.LicenseServer = B1License;
                NewCom.SLDServer = B1SLD;

                NewCom.Connect();
                return NewCom;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}