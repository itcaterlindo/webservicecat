﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http.Results;
using System.Xml.Linq;

namespace WebServiceCAT.SAP
{
    public class MessageInfo
    {
        public int ErrorCode { get; set; }
        public string Message { get; set; }
    }

    public class Function
    {
        CultureInfo English = new CultureInfo("en-US");
        static string DateFormat = "yyyyMMdd";
        static string DecimalSeparator = ",";
        static int DecimalPlaces = 2;

        public static bool ExecuteNonQuery(SAPbobsCOM.Company oCompany, string Qry)
        {
            SAPbobsCOM.Recordset rs = null;
            try
            {
                rs = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                rs.DoQuery(Qry);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool ExecuteScalar(SAPbobsCOM.Company oCompany, string query, ref string value)
        {
            try
            {
                Recordset rs = null;
                value = string.Empty;
                rs = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                    value = rs.Fields.Item(0).Value.ToString().Trim();
                rs = null;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool OpenTable(SAPbobsCOM.Company oCompany, string query, ref DataTable dt)
        {
            try
            {
                Recordset rs = null;
                dt = new DataTable();
                rs = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);

                List<string> ColName = new List<string>();
                List<BoFieldTypes> ColType = new List<BoFieldTypes>();
                for (int i = 0; i < rs.Fields.Count; i++)
                {
                    ColName.Add(rs.Fields.Item(i).Name);
                    ColType.Add(rs.Fields.Item(i).Type);
                }

                XDocument xDoc = XDocument.Parse(rs.GetAsXML());
                XElement xEle = xDoc.Element("BOM").Element("BO");
                var nodes = xEle.Nodes();
                string NodeName = "";
                foreach (XNode node in nodes)
                {
                    if (!node.ToString().ToLower().Contains("adminfo"))
                    {
                        NodeName = node.ToString().Split('>')[0].Replace("<", "").Replace(" /", "");
                        break;
                    }
                }
                xEle = xDoc.Element("BOM").Element("BO").Element(NodeName);
                nodes = xEle.Element("row").Nodes();
                int idx = 0;
                foreach (XNode node in nodes)
                {
                    string FieldName = ColName[idx];
                    dt.Columns.Add(FieldName);

                    if (ColType[idx] == BoFieldTypes.db_Alpha || ColType[idx] == BoFieldTypes.db_Memo)
                        dt.Columns[FieldName].DataType = typeof(string);
                    else if (ColType[idx] == BoFieldTypes.db_Date)
                        dt.Columns[FieldName].DataType = typeof(DateTime);
                    else if (ColType[idx] == BoFieldTypes.db_Float)
                        dt.Columns[FieldName].DataType = typeof(decimal);
                    else if (ColType[idx] == BoFieldTypes.db_Numeric)
                        dt.Columns[FieldName].DataType = typeof(int);

                    idx++;
                }

                if (!rs.EoF)
                {
                    foreach (var item in xEle.Elements("row"))
                    {
                        List<XElement> indexedElements = item.Elements().ToList();
                        dynamic[] row = new dynamic[] { };
                        idx = 0;
                        foreach (System.Data.DataColumn FieldName in dt.Columns)
                        {
                            Array.Resize(ref row, row.Length + 1);
                            if (ColType[idx] == BoFieldTypes.db_Alpha || ColType[idx] == BoFieldTypes.db_Memo)
                                row[row.Length - 1] = item.Element(indexedElements[idx].Name).Value;
                            else if (ColType[idx] == BoFieldTypes.db_Date)
                                row[row.Length - 1] = StringToDateTimeRS(item.Element(indexedElements[idx].Name).Value);
                            else if (ColType[idx] == BoFieldTypes.db_Float)
                                row[row.Length - 1] = StringToDecimalRS(item.Element(indexedElements[idx].Name).Value);
                            else if (ColType[idx] == BoFieldTypes.db_Numeric)
                                row[row.Length - 1] = item.Element(indexedElements[idx].Name).Value;
                            idx++;
                        }
                        dt.Rows.Add(row);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool OpenTable(SAPbobsCOM.Company oCompany, string query, ref DataTable dt, ref string ErrorMessage)
        {
            try
            {
                Recordset rs = null;
                dt = new DataTable();
                rs = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);

                List<string> ColName = new List<string>();
                List<BoFieldTypes> ColType = new List<BoFieldTypes>();
                for (int i = 0; i < rs.Fields.Count; i++)
                {
                    ColName.Add(rs.Fields.Item(i).Name);
                    ColType.Add(rs.Fields.Item(i).Type);
                }

                XDocument xDoc = XDocument.Parse(rs.GetAsXML());
                XElement xEle = xDoc.Element("BOM").Element("BO");
                var nodes = xEle.Nodes();
                string NodeName = "";
                foreach (XNode node in nodes)
                {
                    if (!node.ToString().ToLower().Contains("adminfo"))
                    {
                        NodeName = node.ToString().Split('>')[0].Replace("<", "").Replace(" /", "");
                        break;
                    }
                }
                xEle = xDoc.Element("BOM").Element("BO").Element(NodeName);
                nodes = xEle.Element("row").Nodes();
                int idx = 0;
                foreach (XNode node in nodes)
                {
                    string FieldName = ColName[idx];
                    dt.Columns.Add(FieldName);

                    if (ColType[idx] == BoFieldTypes.db_Alpha || ColType[idx] == BoFieldTypes.db_Memo)
                        dt.Columns[FieldName].DataType = typeof(string);
                    else if (ColType[idx] == BoFieldTypes.db_Date)
                        dt.Columns[FieldName].DataType = typeof(DateTime);
                    else if (ColType[idx] == BoFieldTypes.db_Float)
                        dt.Columns[FieldName].DataType = typeof(decimal);
                    else if (ColType[idx] == BoFieldTypes.db_Numeric)
                        dt.Columns[FieldName].DataType = typeof(int);

                    idx++;
                }

                if (!rs.EoF)
                {
                    foreach (var item in xEle.Elements("row"))
                    {
                        List<XElement> indexedElements = item.Elements().ToList();
                        dynamic[] row = new dynamic[] { };
                        idx = 0;
                        foreach (System.Data.DataColumn FieldName in dt.Columns)
                        {
                            Array.Resize(ref row, row.Length + 1);
                            if (ColType[idx] == BoFieldTypes.db_Alpha || ColType[idx] == BoFieldTypes.db_Memo)
                                row[row.Length - 1] = item.Element(indexedElements[idx].Name).Value;
                            else if (ColType[idx] == BoFieldTypes.db_Date)
                                row[row.Length - 1] = StringToDateTimeRS(item.Element(indexedElements[idx].Name).Value);
                            else if (ColType[idx] == BoFieldTypes.db_Float)
                                row[row.Length - 1] = StringToDecimalRS(item.Element(indexedElements[idx].Name).Value);
                            else if (ColType[idx] == BoFieldTypes.db_Numeric)
                                row[row.Length - 1] = item.Element(indexedElements[idx].Name).Value;
                            idx++;
                        }
                        dt.Rows.Add(row);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.ToString();
                return false;
            }
        }

        public static string Left(string text, int length)
        {
            if (text.Length <= length)
            {
                return text;
            }
            else
            {
                return text.Substring(0, length);
            }
        }

        public static string Right(string Teks, int Length)
        {
            try
            {
                return Teks.Substring(Teks.Length - Length, Length);
            }
            catch
            {
                return "";
            }
        }


        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public static bool StringIsDate(string text)
        {
            try
            {
                if (text.Length == 8)
                {
                    if (text.Split('.').Length == 3)
                    {
                        DateTime dt = new DateTime(Convert.ToInt32(text.Split('.')[2]), Convert.ToInt32(text.Split('.')[1]), Convert.ToInt32(text.Split('.')[0]));
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool StringIsDecimal(string text)
        {
            try
            {
                text = Regex.Replace(text, "[A-Za-z ]", "").Trim();
                decimal val = StringToDecimal(text);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static decimal StringToDecimal(string text)
        {
            if (text.Trim() == "" || text == null)
                return 0;

            text = Regex.Replace(text, "[A-Za-z ]", "").Trim();
            string separator = DecimalSeparator;
            string lawanseparator = "";

            if (separator == ".")
                lawanseparator = ",";
            else
                lawanseparator = ".";

            if (Convert.ToDecimal("3" + separator + "85") == 385)
                return decimal.Round(Convert.ToDecimal(text.Replace(separator, "a").Replace(lawanseparator, separator).Replace("a", lawanseparator)), DecimalPlaces, MidpointRounding.AwayFromZero);
            else
                return decimal.Round(Convert.ToDecimal(text), DecimalPlaces, MidpointRounding.AwayFromZero);
        }

        public static decimal StringToDecimalRS(string text)
        {
            if (text.Trim() == "" || text == null)
                return 0;

            string separator = text.Contains(".") ? "." : ",";
            if (Convert.ToDecimal("3" + separator + "85") == 385)
                return decimal.Round(Convert.ToDecimal(text.Replace(".", "a").Replace(",", ".").Replace("a", ",")), 3, MidpointRounding.AwayFromZero);
            else
                return decimal.Round(Convert.ToDecimal(text), 3, MidpointRounding.AwayFromZero);
        }

        public static double StringToDouble(string text)
        {
            if (text.Trim() == "" || text == null)
                return 0;

            string separator = DecimalSeparator;
            string lawanseparator = "";

            if (separator == ".")
                lawanseparator = ",";
            else
                lawanseparator = ".";

            if (Convert.ToDecimal("3" + separator + "85") == 385)
                return Convert.ToDouble(text.Replace(separator, "a").Replace(lawanseparator, separator).Replace("a", lawanseparator));
            else
                return Convert.ToDouble(text);
        }

        public static string DecimalToString(object text, string currency = "")
        {
            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = DecimalSeparator;
            nfi.CurrencyDecimalDigits = DecimalPlaces;
            nfi.NumberDecimalDigits = DecimalPlaces;

            if (text.ToString() == "NaN")
                return (currency != "" ? currency + " " : "") + string.Format(nfi, "{0:F}", 0);

            return (currency != "" ? currency + " " : "") + string.Format(nfi, "{0:F}", text);
        }

        public static DateTime StringToDateTimeRS(string text)
        {
            if (text == "")
                return DateTime.Now;
            else
            {
                string tahun = Left(text, 4);
                string bulan = Right(Left(text, 6), 2);
                string tanggal = Right(text, 2);
                return Convert.ToDateTime(tahun + "/" + bulan + "/" + tanggal + " 00:00:00");
            }
        }

        public static DateTime StringToDateTime(string text, string format)
        {
            if (text == "")
                return DateTime.Now;
            else
                return DateTime.ParseExact(text, format, System.Globalization.CultureInfo.InvariantCulture);
        }

        public static string DateTimeToString(DateTime date)
        {
            return date.ToString(DateFormat);
        }

        public static string DateTimeToStringSAP(DateTime date)
        {
            return date.ToString("dd.MM.yy");
        }

        public static DateTime StringToDateSAP(string text)
        {
            if (text == "")
                return DateTime.Now;
            else
            {
                string tahun = "20" + text.Split('.')[2];
                string bulan = text.Split('.')[1];
                string tanggal = text.Split('.')[0];
                return new DateTime(Convert.ToInt32(tahun), Convert.ToInt32(bulan), Convert.ToInt32(tanggal));
            }
        }

        public static void CreateLog(string teks)
        {
            Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Log/"));
            using (StreamWriter sw = new StreamWriter(HttpContext.Current.Server.MapPath("~/Log/" + DateTime.Now.ToString("yyyyMMdd") + ".txt"), true))
            {
                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss " + HttpContext.Current.Request.UserHostAddress + " " + teks));
            }
        }
    }
}