﻿using SAPbobsCOM;
using System;
using System.Data;
using System.Runtime.InteropServices;
using WebServiceCAT.Models;

namespace WebServiceCAT.SAP
{
    public class APDownPayment
    {
        public static MessageInfo AddAPDP(SAPbobsCOM.Company oCompany, APDownPaymentModel model)
        {
            string funcname = "AddAPDP";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            Documents oDP = null;

            try
            {
                oCompany.StartTransaction();
                oDP = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oPurchaseDownPayments);
                oDP.UserFields.Fields.Item("U_IDU_APDP_INTNUM").Value = model.U_IDU_APDP_INTNUM;
                oDP.UserFields.Fields.Item("U_FP").Value = model.U_FP;

                string docentrypo = "";
                string pointnum = "";
                Qry = $"SELECT \"DocEntry\", \"U_IDU_PO_INTNUM\" FROM OPOR WHERE \"U_IDU_WEBID\" = '{model.KdPurchaseOrder}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data purchase order dengan kode '{model.KdPurchaseOrder}' tidak ada" };
                }
                docentrypo = dt.Rows[0][0].ToString();
                pointnum = dt.Rows[0][1].ToString();

                oDP.UserFields.Fields.Item("U_IDU_PO_INTNUM").Value = pointnum;
                oDP.DocDate = model.DocDate;
                oDP.DownPaymentType =  DownPaymentTypeEnum.dptInvoice;
                oDP.UserFields.Fields.Item("U_IDU_WEBUSER").Value = model.U_IDU_WEBUSER;
                oDP.DocTotal = model.Amount;

                Qry = $"SELECT \"DocEntry\", \"ObjType\", \"LineNum\" FROM POR1 WHERE \"DocEntry\" = '{docentrypo}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data purchase order dengan docentry '{docentrypo}' tidak ada" };
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    oDP.Lines.BaseEntry = Convert.ToInt32(dt.Rows[i]["DocEntry"].ToString());
                    oDP.Lines.BaseType = Convert.ToInt32(dt.Rows[i]["ObjType"].ToString());
                    oDP.Lines.BaseLine = Convert.ToInt32(dt.Rows[i]["LineNum"].ToString());
                    oDP.Lines.Add();
                }

                if (oDP.Add() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {oCompany.GetNewObjectKey()}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oDP != null)
                {
                    Marshal.ReleaseComObject(oDP);
                    oDP = null;
                }
            }
        }
    }
}