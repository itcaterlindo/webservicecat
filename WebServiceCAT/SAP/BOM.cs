﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using WebServiceCAT.Models;
using WebServiceCAT.SAP;
using System.Data;
using System.Collections;

namespace WebServiceCAT.SAP
{
    public class BOM
    {
        public static MessageInfo AddBOM(Company oCompany, BOM_Header model)
        {
            MessageInfo ret = null;
            ProductTrees oPT = null;
            bool Error = false;

            try
            {
                oCompany.StartTransaction();
                oPT = (ProductTrees)oCompany.GetBusinessObject(BoObjectTypes.oProductTrees);
                oPT.UserFields.Fields.Item("U_IDU_WEBID").Value = model.U_IDU_WEBID;
                oPT.TreeCode = model.Code;
                oPT.ProductDescription = model.Name;
                oPT.Quantity = model.Quantity;

                string RouteStageID = "", AbsEntry = "";
                int IndexStage = 0;

                /** Set WH Code by I_IDU_WEBID */
                string warehouseWebID = model.Warehouse;
                string query = "SELECT \"WhsCode\",\"WhsName\" FROM OWHS WHERE \"U_IDU_WEBID\" = '" + warehouseWebID + "'";
                DataTable dt = new DataTable();
                Function.OpenTable(oCompany, query, ref dt);
                string WHcode = dt.Rows[0]["WhsCode"].ToString();
                oPT.Warehouse = WHcode;
                /** End set WH Code */

                /** Grouping route stage */
                var linesGroupedByRoute = model.Lines.GroupBy(x => x.RouteStage);
                var BomDetail = new List<BOM_Detail>();
                foreach (var group in linesGroupedByRoute)
                {
                    BomDetail.Add(new BOM_Detail { RouteStage = group.Key });
                    foreach (var line in group)
                        BomDetail.Add(new BOM_Detail { ItemType = line.ItemType, ItemCode = line.ItemCode, Quantity = line.Quantity, Warehouse = line.Warehouse, IssueMethod = line.IssueMethod });
                }
                /** End Grouping */

                if (model.TreeType == "P")
                    oPT.TreeType = BoItemTreeTypes.iProductionTree;
                else if (model.TreeType == "A")
                    oPT.TreeType = BoItemTreeTypes.iAssemblyTree;
                else if (model.TreeType == "S")
                    oPT.TreeType = BoItemTreeTypes.iSalesTree;
                else if (model.TreeType == "T")
                    oPT.TreeType = BoItemTreeTypes.iTemplateTree;


                for (int i = 0; i < BomDetail.Count; i++)
                {

                    if (string.IsNullOrWhiteSpace(BomDetail[i].ItemCode))
                    {
                        if (!string.IsNullOrWhiteSpace(BomDetail[i].RouteStage))
                        {
                            /** Set StageRoute Code by U_IDU_WEBID */
                            RouteStageID = BomDetail[i].RouteStage;
                            query = "SELECT * FROM ORST WHERE \"U_IDU_WEBID\" = '" + RouteStageID + "'";
                            Function.OpenTable(oCompany, query, ref dt);
                            AbsEntry = dt.Rows[0]["AbsEntry"].ToString();
                            /** Set StageRoute Code by U_IDU_WEBID */
                            oPT.Stages.StageEntry = Convert.ToInt32(AbsEntry);
                            oPT.Stages.Add();

                        }
                        IndexStage++;
                    }
                    else
                    {

                        /** Set WH Code by U_IDU_WEBID */
                        warehouseWebID = BomDetail[i].Warehouse;
                        query = "SELECT \"WhsCode\",\"WhsName\" FROM OWHS WHERE \"U_IDU_WEBID\" = '" + warehouseWebID + "'";
                        Function.OpenTable(oCompany, query, ref dt);
                        WHcode = dt.Rows[0]["WhsCode"].ToString();
                        /** Set WH Code by U_IDU_WEBID */

                        if (BomDetail[i].ItemType == "I")
                            oPT.Items.ItemType = ProductionItemType.pit_Item;
                        else if (BomDetail[i].ItemType == "R")
                            oPT.Items.ItemType = ProductionItemType.pit_Resource;
                        else if (BomDetail[i].ItemType == "T")
                            oPT.Items.ItemType = ProductionItemType.pit_Text;

                        oPT.Items.ItemCode = BomDetail[i].ItemCode;
                        oPT.Items.Quantity = BomDetail[i].Quantity;
                        oPT.Items.Warehouse = WHcode;

                        if (BomDetail[i].IssueMethod == "M")
                            oPT.Items.IssueMethod = BoIssueMethod.im_Manual;
                        else if (BomDetail[i].ItemType == "B")
                            oPT.Items.IssueMethod = BoIssueMethod.im_Backflush;

                        oPT.Items.StageID = IndexStage;
                        oPT.Items.Add();
                    }

                }

                if (oPT.Add() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = "Failed - [AddBOM] " + oCompany.GetLastErrorDescription() };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = "Success - [AddBOM]" };

            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 2, Message = "Failed - [AddBOM] " + ex.Message.ToString() };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oPT != null)
                {
                    Marshal.ReleaseComObject(oPT);
                    oPT = null;
                }
            }
        }

        public static MessageInfo EditBOM(Company oCompany, BOM_Header model)
        {
            MessageInfo ret = null;
            ProductTrees oPT = null;
            bool Error = false;

            try
            {
                oCompany.StartTransaction();
                oPT = (ProductTrees)oCompany.GetBusinessObject(BoObjectTypes.oProductTrees);

                /** Iniate all variable */
                DataTable dt = new DataTable();
                string warehouseWebID = "";
                string query = "";
                string RouteStageID = "", AbsEntry = "";
                int IndexStage = 0;


                query = "SELECT \"Code\" FROM OITT WHERE \"U_IDU_WEBID\" = '" + model.U_IDU_WEBID + "'";
                Function.OpenTable(oCompany, query, ref dt);
                string DocumentKey = dt.Rows[0]["Code"].ToString();
                if (oPT.GetByKey(DocumentKey))
                {

                    /** Delete all Lines */
                    for (int i = 0; i < oPT.Items.Count; i++)
                    {
                        oPT.Items.SetCurrentLine(i);
                        oPT.Items.Delete();
                    }


                    oPT.UserFields.Fields.Item("U_IDU_WEBID").Value = model.U_IDU_WEBID;
                    // oPT.TreeCode = model.Code;
                    oPT.ProductDescription = model.Name;
                    oPT.Quantity = model.Quantity;


                    /** Set WH Code by U_IDU_WEBID */
                    warehouseWebID = model.Warehouse;
                    query = "SELECT \"WhsCode\",\"WhsName\" FROM OWHS WHERE \"U_IDU_WEBID\" = '" + warehouseWebID + "'";
                    Function.OpenTable(oCompany, query, ref dt);
                    string WHcode = dt.Rows[0]["WhsCode"].ToString();
                    oPT.Warehouse = WHcode;
                    /** End set WH Code */

                    /** Grouping route stage */
                    var linesGroupedByRoute = model.Lines.GroupBy(x => x.RouteStage);
                    var BomDetail = new List<BOM_Detail>();
                    foreach (var group in linesGroupedByRoute)
                    {
                        BomDetail.Add(new BOM_Detail { RouteStage = group.Key });
                        foreach (var line in group)
                            BomDetail.Add(new BOM_Detail { ItemType = line.ItemType, ItemCode = line.ItemCode, Quantity = line.Quantity, Warehouse = line.Warehouse, IssueMethod = line.IssueMethod });
                    }
                    /** End Grouping */

                    if (model.TreeType == "P")
                        oPT.TreeType = BoItemTreeTypes.iProductionTree;
                    else if (model.TreeType == "A")
                        oPT.TreeType = BoItemTreeTypes.iAssemblyTree;
                    else if (model.TreeType == "S")
                        oPT.TreeType = BoItemTreeTypes.iSalesTree;
                    else if (model.TreeType == "T")
                        oPT.TreeType = BoItemTreeTypes.iTemplateTree;


                    for (int i = 0; i < BomDetail.Count; i++)
                    {

                        if (string.IsNullOrWhiteSpace(BomDetail[i].ItemCode))
                        {
                            if (!string.IsNullOrWhiteSpace(BomDetail[i].RouteStage))
                            {
                                /** Set StageRoute Code by U_IDU_WEBID */
                                RouteStageID = BomDetail[i].RouteStage;
                                query = "SELECT * FROM ORST WHERE \"U_IDU_WEBID\" = '" + RouteStageID + "'";
                                Function.OpenTable(oCompany, query, ref dt);
                                AbsEntry = dt.Rows[0]["AbsEntry"].ToString();
                                /** Set StageRoute Code by U_IDU_WEBID */
                                oPT.Stages.StageEntry = Convert.ToInt32(AbsEntry);
                                oPT.Stages.Add();

                            }
                            IndexStage++;
                        }
                        else
                        {

                            /** Set WH Code by U_IDU_WEBID */
                            warehouseWebID = BomDetail[i].Warehouse;
                            query = "SELECT \"WhsCode\",\"WhsName\" FROM OWHS WHERE \"U_IDU_WEBID\" = '" + warehouseWebID + "'";
                            Function.OpenTable(oCompany, query, ref dt);
                            WHcode = dt.Rows[0]["WhsCode"].ToString();
                            /** Set WH Code by U_IDU_WEBID */

                            if (BomDetail[i].ItemType == "I")
                                oPT.Items.ItemType = ProductionItemType.pit_Item;
                            else if (BomDetail[i].ItemType == "R")
                                oPT.Items.ItemType = ProductionItemType.pit_Resource;
                            else if (BomDetail[i].ItemType == "T")
                                oPT.Items.ItemType = ProductionItemType.pit_Text;

                            oPT.Items.ItemCode = BomDetail[i].ItemCode;
                            oPT.Items.Quantity = BomDetail[i].Quantity;
                            oPT.Items.Warehouse = WHcode;

                            if (BomDetail[i].IssueMethod == "M")
                                oPT.Items.IssueMethod = BoIssueMethod.im_Manual;
                            else if (BomDetail[i].ItemType == "B")
                                oPT.Items.IssueMethod = BoIssueMethod.im_Backflush;

                            oPT.Items.StageID = IndexStage;
                            oPT.Items.Add();
                        }

                    }

                }
                else
                {
                    /** Kode BOM WEB tidak ditemukan */
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 1, Message = "Failed - [EditBOM] - Data BOM " + model.Code + " tidak ditemukan !! " + oCompany.GetLastErrorDescription() };
                }



                if (oPT.Update() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = "Failed - [EditBOM] " + oCompany.GetLastErrorDescription() };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = "Success - [EditBOM]" };

            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 2, Message = "Failed - [EditBOM] " + ex.Message.ToString() };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oPT != null)
                {
                    Marshal.ReleaseComObject(oPT);
                    oPT = null;
                }
            }
        }


    }

}