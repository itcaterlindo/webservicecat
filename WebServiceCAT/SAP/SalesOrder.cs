﻿using Microsoft.Win32;
using SAPbobsCOM;
using System;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using WebServiceCAT.Models;

namespace WebServiceCAT.SAP
{
    public class SalesOrder
    {
        public static MessageInfo AddSalesOrder(SAPbobsCOM.Company oCompany, SalesOrderModel model)
        {
            string funcname = "AddSalesOrder";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            Documents oSO = null;

            try
            {
                #region Cek Nomor Sama
                Qry = $"SELECT \"DocEntry\" FROM ORDR WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID.Trim()}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count > 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data sales order dengan kode '{model.U_IDU_WEBID}' sudah ada" };
                }
                #endregion

                oCompany.StartTransaction();
                oSO = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oOrders);
                oSO.UserFields.Fields.Item("U_IDU_WEBID").Value = model.U_IDU_WEBID;

                Qry = $"SELECT \"SlpCode\" FROM OSLP WHERE \"U_IDU_WEBID\" = '{model.KdSalesPerson}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data sales person dengan kode '{model.KdSalesPerson}' tidak ada" };
                }
                oSO.SalesPersonCode = Convert.ToInt32(dt.Rows[0][0].ToString());

                oSO.UserFields.Fields.Item("U_IDU_SO_INTNUM").Value = model.U_IDU_SO_INTNUM;
                oSO.DocDate = model.DocDate;
                oSO.DocDueDate = model.DocDueDate;
                oSO.NumAtCard = model.NumAtCard;
                oSO.Comments = model.Comments;
                if (!string.IsNullOrWhiteSpace(model.U_IDU_PO_INTNUM))
                    oSO.UserFields.Fields.Item("U_IDU_PO_INTNUM").Value = model.U_IDU_PO_INTNUM;
                if (model.U_IDU_PO_Date != null)
                    oSO.UserFields.Fields.Item("U_IDU_PO_Date").Value = model.U_IDU_PO_Date;
                oSO.Address2 = model.Street;

                Qry = $"SELECT \"ExpnsCode\" FROM OEXD WHERE LOWER(\"ExpnsName\") = 'ongkos kirim'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data freight ongkos kirim tidak ada" };
                }
                oSO.Expenses.ExpenseCode = Convert.ToInt32(dt.Rows[0][0]);
                oSO.Expenses.LineTotal = model.OngkosKirim;
                oSO.Expenses.Add();

                Qry = $"SELECT \"ExpnsCode\" FROM OEXD WHERE LOWER(\"ExpnsName\") = 'biaya install'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data freight biaya install tidak ada" };
                }
                oSO.Expenses.ExpenseCode = Convert.ToInt32(dt.Rows[0][0]);
                oSO.Expenses.LineTotal = model.BiayaInstall;
                oSO.Expenses.Add();

                oSO.UserFields.Fields.Item("U_IDU_WEBUSER").Value = model.U_IDU_WEBUSER;

                Qry = $"SELECT T0.\"CardCode\", T1.\"U_IDU_GroupNum\", T1.\"U_IDU_VatGroup\" FROM OCRD T0 LEFT JOIN \"@JENIS_CUSTOMER\" T1 ON T0.\"U_IDU_KodeJenisCustomer\" = T1.\"Code\" WHERE T0.\"CardCode\" = '{model.CardCode}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data jenis customer dengan CardCode '{model.CardCode}' tidak ada" };
                }
                string cardcode = dt.Rows[0]["CardCode"].ToString();
                string groupnum = dt.Rows[0]["U_IDU_GroupNum"].ToString();
                string vatgroup = dt.Rows[0]["U_IDU_VatGroup"].ToString();

                oSO.CardCode = cardcode;
                if (!string.IsNullOrWhiteSpace(groupnum))
                    oSO.GroupNumber = Convert.ToInt32(groupnum);

                for (int i = 0; i < model.Lines_Detail_Item.Count; i++)
                {
                    oSO.Lines.UserFields.Fields.Item("U_IDU_WEBID").Value = model.Lines_Detail_Item[i].U_IDU_WEBID;
                    oSO.Lines.ItemCode = model.Lines_Detail_Item[i].ItemCode;
                    oSO.Lines.ItemDescription = model.Lines_Detail_Item[i].Dscription;

                    #region Cek Warehouse
                    Qry = $"SELECT \"WhsCode\" FROM OWHS WHERE \"U_IDU_WEBID\" = '{model.Lines_Detail_Item[i].KdWarehouse}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data warehouse dengan kode web '{model.Lines_Detail_Item[i].KdWarehouse}' tidak ada" };
                    }
                    string whscode = dt.Rows[0][0].ToString();
                    #endregion

                    oSO.Lines.WarehouseCode = whscode;

                    #region Cek Currency
                    Qry = $"SELECT \"CurrCode\" FROM OCRN WHERE \"U_IDU_WEBID\" = '{model.Lines_Detail_Item[i].KdCurrency}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data currency dengan kode web '{model.Lines_Detail_Item[i].KdCurrency}' tidak ada" };
                    }
                    string currcode = dt.Rows[0][0].ToString();
                    #endregion

                    oSO.Lines.Currency = currcode;

                    oSO.Lines.Quantity = model.Lines_Detail_Item[i].Quantity;

                    #region Cek Uom
                    Qry = $"SELECT \"UomEntry\" FROM OUOM WHERE \"U_IDU_WEBID\" = '{model.Lines_Detail_Item[i].KdSatuan}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data satuan dengan kode web '{model.Lines_Detail_Item[i].KdSatuan}' tidak ada" };
                    }
                    string uomentry = dt.Rows[0][0].ToString();
                    #endregion
                    oSO.Lines.UoMEntry = Convert.ToInt32(uomentry);
                    oSO.Lines.UnitPrice = model.Lines_Detail_Item[i].UnitPrice;
                    if (model.Lines_Detail_Item[i].ItemDisc > 0)
                    {
                        if (model.Lines_Detail_Item[i].DiscType.ToLower().Contains("percent"))
                            oSO.Lines.DiscountPercent = model.Lines_Detail_Item[i].ItemDisc;
                        else
                            oSO.Lines.Price = model.Lines_Detail_Item[i].DiscPrcnt;
                    }
                    if (model.Lines_Detail_Item[i].DiscPrcnt > 0)
                        oSO.Lines.DiscountPercent = model.Lines_Detail_Item[i].DiscPrcnt;

                    if (!string.IsNullOrWhiteSpace(vatgroup))
                        oSO.Lines.VatGroup = vatgroup;

                    oSO.Lines.LineTotal = model.Lines_Detail_Item[i].LineTotal;
                    oSO.Lines.UserFields.Fields.Item("U_IDU_FreeText").Value = model.Lines_Detail_Item[i].U_IDU_FreeText;
                    oSO.Lines.UserFields.Fields.Item("U_IDU_OrderType").Value = model.Lines_Detail_Item[i].U_IDU_OrderType;
                    oSO.Lines.Add();
                }

                for (int i = 0; i < model.Lines_Sub_Detail_Item.Count; i++)
                {
                    oSO.Lines.UserFields.Fields.Item("U_IDU_WEBID").Value = model.Lines_Sub_Detail_Item[i].U_IDU_WEBID;
                    oSO.Lines.ItemCode = model.Lines_Sub_Detail_Item[i].ItemCode;
                    oSO.Lines.ItemDescription = model.Lines_Sub_Detail_Item[i].Dscription;

                    #region Cek Warehouse
                    Qry = $"SELECT \"WhsCode\" FROM OWHS WHERE \"U_IDU_WEBID\" = '{model.Lines_Sub_Detail_Item[i].KdWarehouse}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data warehouse dengan kode web '{model.Lines_Sub_Detail_Item[i].KdWarehouse}' tidak ada" };
                    }
                    string whscode = dt.Rows[0][0].ToString();
                    #endregion

                    oSO.Lines.WarehouseCode = whscode;

                    #region Cek Currency
                    Qry = $"SELECT \"CurrCode\" FROM OCRN WHERE \"U_IDU_WEBID\" = '{model.Lines_Sub_Detail_Item[i].KdCurrency}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data currency dengan kode web '{model.Lines_Sub_Detail_Item[i].KdCurrency}' tidak ada" };
                    }
                    string currcode = dt.Rows[0][0].ToString();
                    #endregion

                    oSO.Lines.Currency = currcode;
                    oSO.DocCurrency = currcode;

                    oSO.Lines.Quantity = model.Lines_Sub_Detail_Item[i].Quantity;

                    #region Cek Uom
                    Qry = $"SELECT \"UomEntry\" FROM OUOM WHERE \"U_IDU_WEBID\" = '{model.Lines_Sub_Detail_Item[i].KdSatuan}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data satuan dengan kode web '{model.Lines_Sub_Detail_Item[i].KdSatuan}' tidak ada" };
                    }
                    string uomentry = dt.Rows[0][0].ToString();
                    #endregion

                    oSO.Lines.UoMEntry = Convert.ToInt32(uomentry);
                    oSO.Lines.UnitPrice = model.Lines_Sub_Detail_Item[i].UnitPrice;
                    if (model.Lines_Sub_Detail_Item[i].ItemDisc > 0)
                    {
                        if (model.Lines_Sub_Detail_Item[i].DiscType.ToLower().Contains("percent"))
                            oSO.Lines.DiscountPercent = model.Lines_Sub_Detail_Item[i].ItemDisc;
                        else
                            oSO.Lines.Price = model.Lines_Sub_Detail_Item[i].DiscPrcnt;
                    }
                    if (model.Lines_Sub_Detail_Item[i].DiscPrcnt > 0)
                        oSO.Lines.DiscountPercent = model.Lines_Sub_Detail_Item[i].DiscPrcnt;

                    if (!string.IsNullOrWhiteSpace(vatgroup))
                        oSO.Lines.VatGroup = vatgroup;

                    oSO.Lines.LineTotal = model.Lines_Sub_Detail_Item[i].LineTotal;
                    oSO.Lines.UserFields.Fields.Item("U_IDU_FreeText").Value = model.Lines_Sub_Detail_Item[i].U_IDU_FreeText;
                    oSO.Lines.UserFields.Fields.Item("U_IDU_OrderType").Value = model.Lines_Sub_Detail_Item[i].U_IDU_OrderType;
                    oSO.Lines.Add();
                }

                oSO.DocTotal = model.DocTotal;

                if (oSO.Add() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                string docentryso = oCompany.GetNewObjectKey();

                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {docentryso}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oSO != null)
                {
                    Marshal.ReleaseComObject(oSO);
                    oSO = null;
                }
            }
        }

        public static MessageInfo EditSalesOrder(SAPbobsCOM.Company oCompany, SalesOrderModel model)
        {
            string funcname = "EditSalesOrder";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            Documents oSO = null;

            try
            {
                #region Cek SO Close
                Qry = $"SELECT \"DocEntry\" FROM ORDR WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID.Trim()}' AND \"DocStatus\" = 'C'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count > 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data sales order dengan kode '{model.U_IDU_WEBID}' sudah closed" };
                }
                #endregion

                #region Ambil DocEntry
                Qry = $"SELECT \"DocEntry\" FROM ORDR WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID.Trim()}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data sales order dengan kode '{model.U_IDU_WEBID}' tidak ada" };
                }
                string docentry = dt.Rows[0][0].ToString();
                #endregion

                oCompany.StartTransaction();
                oSO = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oOrders);

                if (!oSO.GetByKey(Convert.ToInt32(docentry)))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }

                Qry = $"SELECT \"SlpCode\" FROM OSLP WHERE \"U_IDU_WEBID\" = '{model.KdSalesPerson}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data sales person dengan kode '{model.KdSalesPerson}' tidak ada" };
                }
                oSO.SalesPersonCode = Convert.ToInt32(dt.Rows[0][0].ToString());

                oSO.UserFields.Fields.Item("U_IDU_SO_INTNUM").Value = model.U_IDU_SO_INTNUM;
                oSO.DocDate = model.DocDate;
                oSO.DocDueDate = model.DocDueDate;
                oSO.NumAtCard = model.NumAtCard;
                oSO.Comments = model.Comments;
                if (!string.IsNullOrWhiteSpace(model.U_IDU_PO_INTNUM))
                    oSO.UserFields.Fields.Item("U_IDU_PO_INTNUM").Value = model.U_IDU_PO_INTNUM;
                if (model.U_IDU_PO_Date != null)
                    oSO.UserFields.Fields.Item("U_IDU_PO_Date").Value = model.U_IDU_PO_Date;
                oSO.Address2 = model.Street;

                oSO.UserFields.Fields.Item("U_IDU_WEBUSER").Value = model.U_IDU_WEBUSER;

                Qry = $"SELECT T0.\"CardCode\", T1.\"U_IDU_GroupNum\", T1.\"U_IDU_VatGroup\" FROM OCRD T0 LEFT JOIN \"@JENIS_CUSTOMER\" T1 ON T0.\"U_IDU_KodeJenisCustomer\" = T1.\"Code\" WHERE T0.\"CardCode\" = '{model.CardCode}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data jenis customer dengan CardCode '{model.CardCode}' tidak ada" };
                }
                string cardcode = dt.Rows[0]["CardCode"].ToString();
                string groupnum = dt.Rows[0]["U_IDU_GroupNum"].ToString();
                string vatgroup = dt.Rows[0]["U_IDU_VatGroup"].ToString();

                oSO.CardCode = cardcode;
                if (!string.IsNullOrWhiteSpace(groupnum))
                    oSO.GroupNumber = Convert.ToInt32(groupnum);

                for (int i = 0; i < model.Lines_Detail_Item.Count; i++)
                {
                    Qry = $"SELECT \"DocEntry\" FROM RDR1 WHERE \"DocEntry\" = '{docentry}' AND \"U_IDU_WEBID\" = '{model.Lines_Detail_Item[i].U_IDU_WEBID}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count > 0)
                    {
                        continue;
                    }

                    oSO.Lines.Add();
                    oSO.Lines.UserFields.Fields.Item("U_IDU_WEBID").Value = model.Lines_Detail_Item[i].U_IDU_WEBID;
                    oSO.Lines.ItemCode = model.Lines_Detail_Item[i].ItemCode;
                    oSO.Lines.ItemDescription = model.Lines_Detail_Item[i].Dscription;

                    #region Cek Warehouse
                    Qry = $"SELECT \"WhsCode\" FROM OWHS WHERE \"U_IDU_WEBID\" = '{model.Lines_Detail_Item[i].KdWarehouse}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data warehouse dengan kode web '{model.Lines_Detail_Item[i].KdWarehouse}' tidak ada" };
                    }
                    string whscode = dt.Rows[0][0].ToString();
                    #endregion

                    oSO.Lines.WarehouseCode = whscode;
                    oSO.Lines.Quantity = model.Lines_Detail_Item[i].Quantity;

                    #region Cek Uom
                    Qry = $"SELECT \"UomEntry\" FROM OUOM WHERE \"U_IDU_WEBID\" = '{model.Lines_Detail_Item[i].KdSatuan}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data satuan dengan kode web '{model.Lines_Detail_Item[i].KdSatuan}' tidak ada" };
                    }
                    string uomentry = dt.Rows[0][0].ToString();
                    #endregion

                    oSO.Lines.UoMEntry = Convert.ToInt32(uomentry);
                    oSO.Lines.UnitPrice = model.Lines_Detail_Item[i].UnitPrice;
                    oSO.Lines.DiscountPercent = model.Lines_Detail_Item[i].DiscPrcnt;

                    if (model.Lines_Detail_Item[i].DiscType.ToLower().Contains("percent"))
                        oSO.Lines.DiscountPercent = model.Lines_Detail_Item[i].ItemDisc;
                    else
                        oSO.Lines.Price = model.Lines_Detail_Item[i].UnitPrice - model.Lines_Detail_Item[i].ItemDisc;

                    if (!string.IsNullOrWhiteSpace(vatgroup))
                        oSO.Lines.VatGroup = vatgroup;

                    oSO.Lines.LineTotal = model.Lines_Detail_Item[i].LineTotal;
                    oSO.Lines.UserFields.Fields.Item("U_IDU_FreeText").Value = model.Lines_Detail_Item[i].U_IDU_FreeText;
                    oSO.Lines.UserFields.Fields.Item("U_IDU_OrderType").Value = model.Lines_Detail_Item[i].U_IDU_OrderType;
                }

                for (int i = 0; i < model.Lines_Sub_Detail_Item.Count; i++)
                {
                    Qry = $"SELECT \"DocEntry\" FROM RDR1 WHERE \"DocEntry\" = '{docentry}' AND \"U_IDU_WEBID\" = '{model.Lines_Sub_Detail_Item[i].U_IDU_WEBID}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count > 0)
                    {
                        continue;
                    }

                    oSO.Lines.Add();
                    oSO.Lines.UserFields.Fields.Item("U_IDU_WEBID").Value = model.Lines_Sub_Detail_Item[i].U_IDU_WEBID;
                    oSO.Lines.ItemCode = model.Lines_Sub_Detail_Item[i].ItemCode;
                    oSO.Lines.ItemDescription = model.Lines_Sub_Detail_Item[i].Dscription;

                    #region Cek Warehouse
                    Qry = $"SELECT \"WhsCode\" FROM OWHS WHERE \"U_IDU_WEBID\" = '{model.Lines_Sub_Detail_Item[i].KdWarehouse}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data warehouse dengan kode web '{model.Lines_Sub_Detail_Item[i].KdWarehouse}' tidak ada" };
                    }
                    string whscode = dt.Rows[0][0].ToString();
                    #endregion

                    oSO.Lines.WarehouseCode = whscode;
                    oSO.Lines.Quantity = model.Lines_Sub_Detail_Item[i].Quantity;

                    #region Cek Uom
                    Qry = $"SELECT \"UomEntry\" FROM OUOM WHERE \"U_IDU_WEBID\" = '{model.Lines_Sub_Detail_Item[i].KdSatuan}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data satuan dengan kode web '{model.Lines_Sub_Detail_Item[i].KdSatuan}' tidak ada" };
                    }
                    string uomentry = dt.Rows[0][0].ToString();
                    #endregion

                    oSO.Lines.UoMEntry = Convert.ToInt32(uomentry);
                    oSO.Lines.UnitPrice = model.Lines_Sub_Detail_Item[i].UnitPrice;
                    oSO.Lines.DiscountPercent = model.Lines_Sub_Detail_Item[i].DiscPrcnt;

                    if (model.Lines_Sub_Detail_Item[i].DiscType.ToLower().Contains("percent"))
                        oSO.Lines.DiscountPercent = model.Lines_Sub_Detail_Item[i].ItemDisc;
                    else
                        oSO.Lines.Price = model.Lines_Sub_Detail_Item[i].UnitPrice - model.Lines_Sub_Detail_Item[i].ItemDisc;

                    if (!string.IsNullOrWhiteSpace(vatgroup))
                        oSO.Lines.VatGroup = vatgroup;

                    oSO.Lines.LineTotal = model.Lines_Sub_Detail_Item[i].LineTotal;
                    oSO.Lines.UserFields.Fields.Item("U_IDU_FreeText").Value = model.Lines_Sub_Detail_Item[i].U_IDU_FreeText;
                    oSO.Lines.UserFields.Fields.Item("U_IDU_OrderType").Value = model.Lines_Sub_Detail_Item[i].U_IDU_OrderType;
                }

                oSO.DocTotal = model.DocTotal;

                if (oSO.Update() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                string docentryso = oCompany.GetNewObjectKey();
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {docentryso}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oSO != null)
                {
                    Marshal.ReleaseComObject(oSO);
                    oSO = null;
                }
            }
        }

        public static MessageInfo CloseSalesOrder(SAPbobsCOM.Company oCompany, SalesOrderModel model)
        {
            string funcname = "CloseSalesOrder";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            Documents oSO = null;

            try
            {
                #region Ambil DocEntry
                Qry = $"SELECT \"DocEntry\" FROM ORDR WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data sales order dengan kode web '{model.U_IDU_WEBID}' tidak ada" };
                }
                string docentry = dt.Rows[0][0].ToString();
                #endregion

                #region Cek Sales Order Closed
                Qry = $"SELECT \"DocEntry\" FROM ORDR WHERE \"DocEntry\" = '{docentry}' AND \"DocStatus\" = 'O'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data sales order dengan kode web '{model.U_IDU_WEBID}' sudah closed" };
                }
                #endregion

                oCompany.StartTransaction();
                oSO = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oOrders);
                if (!oSO.GetByKey(Convert.ToInt32(docentry)))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }

                if (oSO.Close() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {model.U_IDU_WEBID}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oSO != null)
                {
                    Marshal.ReleaseComObject(oSO);
                    oSO = null;
                }
            }
        }

        public static MessageInfo CancelSalesOrder(SAPbobsCOM.Company oCompany, SalesOrderModel model)
        {
            string funcname = "CancelSalesOrder";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            Documents oSO = null;

            try
            {
                #region Ambil DocEntry
                Qry = $"SELECT \"DocEntry\" FROM ORDR WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data sales order dengan kode web '{model.U_IDU_WEBID}' tidak ada" };
                }
                string docentry = dt.Rows[0][0].ToString();
                #endregion

                oCompany.StartTransaction();
                oSO = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oOrders);
                if (!oSO.GetByKey(Convert.ToInt32(docentry)))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }

                if (oSO.Cancel() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {model.U_IDU_WEBID}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oSO != null)
                {
                    Marshal.ReleaseComObject(oSO);
                    oSO = null;
                }
            }
        }

        public static MessageInfo EditTglKirimSalesOrder(SAPbobsCOM.Company oCompany, SalesOrderModel model)
        {
            string funcname = "EditTglKirimSalesOrder";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            Documents oSO = null;

            try
            {
                #region Cek SO Close
                Qry = $"SELECT \"DocEntry\" FROM ORDR WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID.Trim()}' AND \"DocStatus\" = 'C'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count > 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data sales order dengan kode '{model.U_IDU_WEBID}' sudah closed" };
                }
                #endregion

                #region Ambil DocEntry
                Qry = $"SELECT \"DocEntry\" FROM ORDR WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID.Trim()}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data sales order dengan kode '{model.U_IDU_WEBID}' tidak ada" };
                }
                string docentry = dt.Rows[0][0].ToString();
                #endregion

                oCompany.StartTransaction();
                oSO = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oOrders);

                if (!oSO.GetByKey(Convert.ToInt32(docentry)))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }

                oSO.DocDueDate = model.DocDueDate;
                oSO.UserFields.Fields.Item("U_IDU_WEBUSER").Value = model.U_IDU_WEBUSER;

                if (oSO.Update() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                string docentryso = oCompany.GetNewObjectKey();
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {docentryso}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oSO != null)
                {
                    Marshal.ReleaseComObject(oSO);
                    oSO = null;
                }
            }

        }
    }
}