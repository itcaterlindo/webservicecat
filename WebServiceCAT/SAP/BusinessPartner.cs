﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using WebServiceCAT.Models;

namespace WebServiceCAT.SAP
{
    public class BusinessPartner
    {
        public static MessageInfo AddBP(SAPbobsCOM.Company oCompany, BusinessPartnerModel model)
        {
            string funcname = "AddBP";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            BusinessPartners oBP = null;

            try
            {
                oCompany.StartTransaction();
                oBP = (BusinessPartners)oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartners);
                oBP.CardCode = model.CardCode;
                oBP.CardName = $"{model.CardName}, {model.U_IDU_BadanUsaha}";

                if (model.CardType == "C")
                    oBP.CardType = BoCardTypes.cCustomer;
                else if (model.CardType == "S")
                    oBP.CardType = BoCardTypes.cSupplier;

                #region Ambil GroupCode
                if (model.CardType == "C")
                {
                    Qry = $"SELECT \"GroupCode\" FROM OCRG WHERE \"U_IDU_WEBID\" = '{model.KdGroup}'";
                    if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                    }
                    if (dt.Rows.Count == 0)
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data customer/vendor group dengan kode web '{model.KdGroup}' tidak ada" };
                    }
                    string groupcode = dt.Rows[0][0].ToString();
                    oBP.GroupCode = Convert.ToInt32(groupcode);
                }
                #endregion


                oBP.ContactEmployees.Name = model.CntctPrsn;
                oBP.ContactEmployees.Add();

                oBP.ContactPerson = model.CntctPrsn;
                oBP.FederalTaxID = model.LicTradNum;

                oBP.DebitorAccount = model.DebPayAcct;

                oBP.Phone1 = model.Phone1;
                oBP.Phone2 = model.Phone2;
                oBP.EmailAddress = model.Email;
                oBP.Fax = model.Fax;

                oBP.Currency = model.Currency;

                oBP.Addresses.AddressName = Function.Left(model.Street, 50);
                oBP.Addresses.Street = model.Street;
                oBP.Addresses.AddressType = BoAddressType.bo_BillTo;
                oBP.Addresses.ZipCode = model.ZipCode;
                oBP.Addresses.UserFields.Fields.Item("U_IDU_Telp1").Value = model.Phone1;
                oBP.Addresses.UserFields.Fields.Item("U_IDU_Telp2").Value = model.Phone2;
                oBP.Addresses.UserFields.Fields.Item("U_IDU_Email").Value = model.Email;
                oBP.Addresses.UserFields.Fields.Item("U_IDU_Fax").Value = model.Fax;
                oBP.Addresses.UserFields.Fields.Item("U_IDU_BadanUsaha").Value = model.U_IDU_BadanUsaha;
                oBP.Addresses.UserFields.Fields.Item("U_IDU_NamaCustomer").Value = model.CardName;
                oBP.Addresses.UserFields.Fields.Item("U_IDU_CP").Value = model.CntctPrsn;
                oBP.Addresses.Add();

                if (model.CardType == "S")
                {
                    if (string.IsNullOrWhiteSpace(model.VendorGroupNum))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data GroupNum / term of payment harus diisi" };
                    }
                    else
                    {
                        oBP.PayTermsGrpCode = Convert.ToInt32(model.VendorGroupNum);
                    }

                    if (string.IsNullOrWhiteSpace(model.VendorVatGroup))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data GroupNum / term of payment harus diisi" };
                    }
                    else
                    {
                        oBP.VatGroup = model.VendorVatGroup;
                    }
                }

                oBP.UserFields.Fields.Item("U_IDU_WEBUSER").Value = model.U_IDU_WEBUSER.Trim();
                oBP.UserFields.Fields.Item("U_IDU_WEBID").Value = model.U_IDU_WEBID.Trim();
                oBP.UserFields.Fields.Item("U_IDU_KodeJenisCustomer").Value = model.U_IDU_KodeJenisCustomer.Trim();
                oBP.UserFields.Fields.Item("U_IDU_BadanUsaha").Value = model.U_IDU_BadanUsaha.Trim();

                for (int i = 0; i < model.Lines_ShipTo.Count; i++)
                {
                    oBP.Addresses.AddressName = Function.Left(model.Lines_ShipTo[i].Street, 50); //$"{model.Lines_ShipTo[i].U_IDU_NamaCustomer}, {model.Lines_ShipTo[i].U_IDU_BadanUsaha}";
                    oBP.Addresses.Street = model.Lines_ShipTo[i].Street;
                    oBP.Addresses.AddressType = BoAddressType.bo_ShipTo;
                    oBP.Addresses.ZipCode = model.Lines_ShipTo[i].ZipCode;
                    oBP.Addresses.UserFields.Fields.Item("U_IDU_Telp1").Value = model.Lines_ShipTo[i].Phone1;
                    oBP.Addresses.UserFields.Fields.Item("U_IDU_Telp2").Value = model.Lines_ShipTo[i].Phone2;
                    oBP.Addresses.UserFields.Fields.Item("U_IDU_Email").Value = model.Lines_ShipTo[i].Email;
                    oBP.Addresses.UserFields.Fields.Item("U_IDU_Fax").Value = model.Lines_ShipTo[i].Fax;
                    oBP.Addresses.UserFields.Fields.Item("U_IDU_BadanUsaha").Value = model.Lines_ShipTo[i].U_IDU_BadanUsaha;
                    oBP.Addresses.UserFields.Fields.Item("U_IDU_NamaCustomer").Value = model.Lines_ShipTo[i].U_IDU_NamaCustomer;
                    oBP.Addresses.UserFields.Fields.Item("U_IDU_CP").Value = model.Lines_ShipTo[i].U_IDU_CP;
                    oBP.Addresses.Add();
                }

                if (oBP.Add() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {oCompany.GetNewObjectKey()}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oBP != null)
                {
                    Marshal.ReleaseComObject(oBP);
                    oBP = null;
                }
            }
        }

        public static MessageInfo EditBP(SAPbobsCOM.Company oCompany, BusinessPartnerModel model)
        {
            string funcname = "EditBP";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            BusinessPartners oBP = null;

            try
            {
                oCompany.StartTransaction();
                oBP = (BusinessPartners)oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartners);
                if (!oBP.GetByKey(model.CardCode))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                //oCust.CardType = BoCardTypes.cCustomer;
                //oCust.CardCode = model.CardCode;
                oBP.CardName = $"{model.CardName}, {model.U_IDU_BadanUsaha}";

                #region Ambil GroupCode
                Qry = $"SELECT \"GroupCode\" FROM OCRG WHERE \"U_IDU_WEBID\" = '{model.KdGroup}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data customer/vendor group dengan kode web '{model.KdGroup}' tidak ada" };
                }
                string groupcode = dt.Rows[0][0].ToString();
                #endregion

                oBP.GroupCode = Convert.ToInt32(groupcode);

                oBP.ContactEmployees.SetCurrentLine(0);
                oBP.ContactEmployees.Name = model.CntctPrsn;

                oBP.ContactPerson = model.CntctPrsn;
                oBP.FederalTaxID = model.LicTradNum;

                oBP.DebitorAccount = model.DebPayAcct;

                oBP.Phone1 = model.Phone1;
                oBP.Phone2 = model.Phone2;
                oBP.EmailAddress = model.Email;
                oBP.Fax = model.Fax;

                oBP.Currency = model.Currency;

                for (int i = oBP.Addresses.Count - 1; i >= 0; i--)
                {
                    oBP.Addresses.SetCurrentLine(i);
                    oBP.Addresses.Delete();
                }

                oBP.Addresses.AddressName = Function.Left(model.Street, 50); //$"{model.CardName}, {model.U_IDU_BadanUsaha}";
                oBP.Addresses.Street = model.Street;
                oBP.Addresses.AddressType = BoAddressType.bo_BillTo;
                oBP.Addresses.ZipCode = model.ZipCode;
                oBP.Addresses.UserFields.Fields.Item("U_IDU_Telp1").Value = model.Phone1;
                oBP.Addresses.UserFields.Fields.Item("U_IDU_Telp2").Value = model.Phone2;
                oBP.Addresses.UserFields.Fields.Item("U_IDU_Email").Value = model.Email;
                oBP.Addresses.UserFields.Fields.Item("U_IDU_Fax").Value = model.Fax;
                oBP.Addresses.UserFields.Fields.Item("U_IDU_BadanUsaha").Value = model.U_IDU_BadanUsaha;
                oBP.Addresses.UserFields.Fields.Item("U_IDU_NamaCustomer").Value = model.CardName;
                oBP.Addresses.UserFields.Fields.Item("U_IDU_CP").Value = model.CntctPrsn;
                oBP.Addresses.Add();

                if (model.CardType == "S")
                {
                    if (string.IsNullOrWhiteSpace(model.VendorGroupNum))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data GroupNum / term of payment harus diisi" };
                    }
                    else
                    {
                        oBP.PayTermsGrpCode = Convert.ToInt32(model.VendorGroupNum);
                    }

                    if (string.IsNullOrWhiteSpace(model.VendorVatGroup))
                    {
                        Error = true;
                        return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data GroupNum / term of payment harus diisi" };
                    }
                    else
                    {
                        oBP.VatGroup = model.VendorVatGroup;
                    }
                }

                oBP.UserFields.Fields.Item("U_IDU_WEBUSER").Value = model.U_IDU_WEBUSER.Trim();
                oBP.UserFields.Fields.Item("U_IDU_WEBID").Value = model.U_IDU_WEBID.Trim();
                oBP.UserFields.Fields.Item("U_IDU_KodeJenisCustomer").Value = model.U_IDU_KodeJenisCustomer.Trim();
                oBP.UserFields.Fields.Item("U_IDU_BadanUsaha").Value = model.U_IDU_BadanUsaha.Trim();

                for (int i = 0; i < model.Lines_ShipTo.Count; i++)
                {
                    oBP.Addresses.AddressName = Function.Left(model.Lines_ShipTo[i].Street, 50); //$"{model.Lines_ShipTo[i].U_IDU_NamaCustomer}, {model.Lines_ShipTo[i].U_IDU_BadanUsaha}";
                    oBP.Addresses.Street = model.Lines_ShipTo[i].Street;
                    oBP.Addresses.AddressType = BoAddressType.bo_ShipTo;
                    oBP.Addresses.ZipCode = model.Lines_ShipTo[i].ZipCode;
                    oBP.Addresses.UserFields.Fields.Item("U_IDU_Telp1").Value = model.Lines_ShipTo[i].Phone1;
                    oBP.Addresses.UserFields.Fields.Item("U_IDU_Telp2").Value = model.Lines_ShipTo[i].Phone2;
                    oBP.Addresses.UserFields.Fields.Item("U_IDU_Email").Value = model.Lines_ShipTo[i].Email;
                    oBP.Addresses.UserFields.Fields.Item("U_IDU_Fax").Value = model.Lines_ShipTo[i].Fax;
                    oBP.Addresses.UserFields.Fields.Item("U_IDU_BadanUsaha").Value = model.Lines_ShipTo[i].U_IDU_BadanUsaha;
                    oBP.Addresses.UserFields.Fields.Item("U_IDU_NamaCustomer").Value = model.Lines_ShipTo[i].U_IDU_NamaCustomer;
                    oBP.Addresses.UserFields.Fields.Item("U_IDU_CP").Value = model.Lines_ShipTo[i].U_IDU_CP;
                    oBP.Addresses.Add();
                }

                if (oBP.Update() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {oCompany.GetNewObjectKey()}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oBP != null)
                {
                    Marshal.ReleaseComObject(oBP);
                    oBP = null;
                }
            }
        }

        public static object GetBP(SAPbobsCOM.Company oCompany, string CardType)
        {
            string funcname = "GetBP";
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();

            try
            {
                Qry = $"SELECT T0.\"CardCode\", T0.\"CardName\", T0.\"CardType\", T2.\"U_IDU_WEBID\" \"KdGroup\", T0.\"CntctPrsn\", T0.\"CardFName\", T0.\"LicTradNum\", T0.\"DebPayAcct\", T0.\"BillToDef\" \"Address\", T1.\"ZipCode\", T0.\"Phone1\", T0.\"Phone2\", T0.\"E_Mail\", T0.\"Fax\", T0.\"U_IDU_WEBID\", T0.\"U_IDU_WEBUSER\", T0.\"U_IDU_KodeJenisCustomer\", T0.\"U_IDU_BadanUsaha\"{(CardType == "S" ? ", T0.\"GroupNum\", T0.\"ECVatGroup\" \"VatGroup\"" : "")} FROM OCRD T0 LEFT JOIN CRD1 T1 ON T0.\"CardCode\" = T1.\"CardCode\" AND T1.\"AdresType\" = 'B' LEFT JOIN OCRG T2 ON T0.\"GroupCode\" = T2.\"GroupCode\" WHERE COALESCE(T0.\"U_IDU_WEBID\",'') <> '' AND T0.\"CardType\" = '{CardType}' ORDER BY T0.\"CardCode\"";
                Function.OpenTable(oCompany, Qry, ref dt, ref tmp);
                if (tmp != "")
                    return new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };

                return dt;
            }
            catch (Exception ex)
            {
                return new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
        }

        public static object GetBPByID(SAPbobsCOM.Company oCompany, string CardCode)
        {
            string funcname = "GetBPByID";
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();

            try
            {
                Qry = $"SELECT T0.\"CardCode\", T0.\"CardName\", T0.\"CardType\", T2.\"U_IDU_WEBID\" \"KdGroup\", T0.\"CntctPrsn\", T0.\"CardFName\", T0.\"LicTradNum\", T0.\"DebPayAcct\", T0.\"Phone1\", T0.\"Phone2\", T0.\"E_Mail\", T0.\"Fax\", T0.\"Address\", T1.\"ZipCode\", T0.\"GroupNum\", T0.\"ECVatGroup\", T0.\"U_IDU_WEBID\", T0.\"U_IDU_WEBUSER\", T0.\"U_IDU_KodeJenisCustomer\", T0.\"U_IDU_KodeJenisVendor\", T0.\"U_IDU_BadanUsaha\" FROM OCRD T0 LEFT JOIN CRD1 T1 ON T0.\"CardCode\" = T1.\"CardCode\" AND T1.\"AdresType\" = 'B' LEFT JOIN OCRG T2 ON T0.\"GroupCode\" = T2.\"GroupCode\" WHERE T0.\"CardCode\" = '{CardCode}'";
                Function.OpenTable(oCompany, Qry, ref dt, ref tmp);
                if (tmp != "")
                    return new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };

                Qry = $"SELECT \"Street\", \"ZipCode\", \"U_IDU_Telp1\", \"U_IDU_Telp2\", \"U_IDU_Email\", \"U_IDU_Fax\", \"U_IDU_BadanUsaha\", \"U_IDU_NamaCustomer\", \"U_IDU_CP\" FROM CRD1 WHERE \"CardCode\" = '{CardCode}' AND \"AdresType\" = 'S' ORDER BY \"LineNum\"";
                Function.OpenTable(oCompany, Qry, ref dt2, ref tmp);
                if (tmp != "")
                    return new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };

                BusinessPartnerModel header = new BusinessPartnerModel();
                header.CardCode = dt.Rows[0]["CardCode"].ToString();
                header.CardName = dt.Rows[0]["CardName"].ToString();
                header.CardType = dt.Rows[0]["CardType"].ToString();
                header.KdGroup = dt.Rows[0]["KdGroup"].ToString();
                header.CntctPrsn = dt.Rows[0]["CntctPrsn"].ToString();
                header.LicTradNum = dt.Rows[0]["LicTradNum"].ToString();
                header.Phone1 = dt.Rows[0]["Phone1"].ToString();
                header.Phone2 = dt.Rows[0]["Phone2"].ToString();
                header.Email = dt.Rows[0]["E_Mail"].ToString();
                header.Fax = dt.Rows[0]["Fax"].ToString();
                header.Street = dt.Rows[0]["Address"].ToString();
                header.ZipCode = dt.Rows[0]["ZipCode"].ToString();
                header.VendorGroupNum = dt.Rows[0]["GroupNum"].ToString();
                header.VendorVatGroup = dt.Rows[0]["ECVatGroup"].ToString();
                header.U_IDU_WEBID = dt.Rows[0]["U_IDU_WEBID"].ToString();
                header.U_IDU_WEBUSER = dt.Rows[0]["U_IDU_WEBUSER"].ToString();
                header.U_IDU_KodeJenisCustomer = dt.Rows[0]["U_IDU_KodeJenisCustomer"].ToString();
                header.U_IDU_BadanUsaha = dt.Rows[0]["U_IDU_BadanUsaha"].ToString();

                List<BP_ShipTo> shiptoall = new List<BP_ShipTo>();
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    BP_ShipTo shipto = new BP_ShipTo();
                    shipto.Street = dt2.Rows[i]["Street"].ToString();
                    shipto.ZipCode = dt2.Rows[i]["ZipCode"].ToString();
                    shipto.Phone1 = dt2.Rows[i]["U_IDU_Telp1"].ToString();
                    shipto.Phone2 = dt2.Rows[i]["U_IDU_Telp2"].ToString();
                    shipto.Email = dt2.Rows[i]["U_IDU_Email"].ToString();
                    shipto.Fax = dt2.Rows[i]["U_IDU_Fax"].ToString();
                    shipto.U_IDU_BadanUsaha = dt2.Rows[i]["U_IDU_BadanUsaha"].ToString();
                    shipto.U_IDU_NamaCustomer = dt2.Rows[i]["U_IDU_NamaCustomer"].ToString();
                    shipto.U_IDU_CP = dt2.Rows[i]["U_IDU_CP"].ToString();
                    shiptoall.Add(shipto);
                }
                if (shiptoall.Count > 0)
                    header.Lines_ShipTo = shiptoall;

                return header;
            }
            catch (Exception ex)
            {
                return new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
        }
    }
}