﻿using Newtonsoft.Json.Linq;
using SAPbobsCOM;
using System;
using System.Data;
using System.Runtime.InteropServices;
using WebServiceCAT.Models;

namespace WebServiceCAT.SAP
{
    public class SalesPerson
    {
        public static MessageInfo AddSalesPerson(SAPbobsCOM.Company oCompany, SalesPersonModel model)
        {
            string funcname = "AddSalesPerson";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            SalesPersons oSP = null;

            try
            {
                #region Cek Kode Sama
                Qry = $"SELECT \"SlpCode\" FROM OSLP WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID.Trim()}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count > 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data sales person dengan kode '{model.U_IDU_WEBID}' sudah ada" };
                }
                #endregion

                oCompany.StartTransaction();
                oSP = (SalesPersons)oCompany.GetBusinessObject(BoObjectTypes.oSalesPersons);
                oSP.UserFields.Fields.Item("U_IDU_WEBID").Value = model.U_IDU_WEBID.Trim();
                oSP.SalesEmployeeName = model.SlpName.Trim();
                oSP.eMail = model.Email.Trim();
                oSP.Telephone = model.Telp.Trim();

                if (oSP.Add() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {model.U_IDU_WEBID}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oSP != null)
                {
                    Marshal.ReleaseComObject(oSP);
                    oSP = null;
                }
            }
        }

        public static MessageInfo EditSalesPerson(SAPbobsCOM.Company oCompany, SalesPersonModel model)
        {
            string funcname = "EditSalesPerson";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            SalesPersons oSP = null;

            try
            {
                #region Ambil SlpCode
                Qry = $"SELECT \"SlpCode\" FROM OSLP WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID.Trim()}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data sales person dengan kode '{model.U_IDU_WEBID.Trim()}' tidak ditemukan" };
                }
                tmp = dt.Rows[0][0].ToString();
                #endregion

                oCompany.StartTransaction();
                oSP = (SalesPersons)oCompany.GetBusinessObject(BoObjectTypes.oSalesPersons);
                if (!oSP.GetByKey(Convert.ToInt32(tmp)))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }

                oSP.SalesEmployeeName = model.SlpName.Trim();
                oSP.eMail = model.Email.Trim();
                oSP.Telephone = model.Telp.Trim();

                if (oSP.Update() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {model.U_IDU_WEBID.Trim()}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oSP != null)
                {
                    Marshal.ReleaseComObject(oSP);
                    oSP = null;
                }
            }
        }

        public static object GetSalesPerson(SAPbobsCOM.Company oCompany)
        {
            string funcname = "GetSalesPerson";
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();

            try
            {
                Qry = $"SELECT \"U_IDU_WEBID\", \"SlpName\", \"Email\", \"Telephone\" \"Telp\" FROM OSLP WHERE COALESCE(\"U_IDU_WEBID\",'') <> '' ORDER BY \"U_IDU_WEBID\"";
                Function.OpenTable(oCompany, Qry, ref dt, ref tmp);
                if (tmp != "")
                    return new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };

                return dt;
            }
            catch (Exception ex)
            {
                return new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
        }
    }
}