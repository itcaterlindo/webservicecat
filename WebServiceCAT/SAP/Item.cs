﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.InteropServices;
using WebServiceCAT.Models;

namespace WebServiceCAT.SAP
{
    public class Item
    {
        public static MessageInfo AddItem(SAPbobsCOM.Company oCompany, ItemModel model)
        {
            string funcname = "AddItem";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            Items oItem = null;

            try
            {
                #region Cek Kode Sama
                Qry = $"SELECT \"ItemCode\" FROM OITM WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID.Trim()}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count > 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data item dengan kode '{model.U_IDU_WEBID}' sudah ada" };
                }
                #endregion

                oCompany.StartTransaction();
                oItem = (Items)oCompany.GetBusinessObject(BoObjectTypes.oItems);
                oItem.ItemCode = model.ItemCode;
                oItem.ItemName = model.ItemName;

                if (model.PurchaseItem.ToLower() == "y")
                    oItem.PurchaseItem = BoYesNoEnum.tYES;
                else if (model.PurchaseItem.ToLower() == "n")
                    oItem.PurchaseItem = BoYesNoEnum.tNO;

                if (model.SalesItem.ToLower() == "y")
                    oItem.SalesItem = BoYesNoEnum.tYES;
                else if (model.SalesItem.ToLower() == "n")
                    oItem.SalesItem = BoYesNoEnum.tNO;

                if (model.InventoryItem.ToLower() == "y")
                    oItem.InventoryItem = BoYesNoEnum.tYES;
                else if (model.InventoryItem.ToLower() == "n")
                    oItem.InventoryItem = BoYesNoEnum.tNO;

                #region Ambil ItmsGrpCod OITB
                Qry = $"SELECT \"ItmsGrpCod\" FROM OITB WHERE \"U_IDU_WEBID\" = '{model.KdItemGroup}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data item group dengan kode web '{model.KdItemGroup}' tidak ada" };
                }
                string itmsgrpcod = dt.Rows[0][0].ToString();
                #endregion

                oItem.ItemsGroupCode = Convert.ToInt32(itmsgrpcod);

                oItem.PurchaseUnitLength = model.Length;
                oItem.PurchaseUnitWidth = model.Width;
                oItem.PurchaseUnitHeight = model.Height;
                oItem.PurchaseUnitVolume = model.Volume;

                oItem.SalesUnitLength = model.Length;
                oItem.SalesUnitWidth = model.Width;
                oItem.SalesUnitHeight = model.Height;
                oItem.SalesUnitVolume = model.Volume;

                oItem.InventoryWeight = model.GrossWeight;

                if (!string.IsNullOrWhiteSpace(model.PrchseItem))
                {
                    if (model.PrchseItem.ToLower() == "y")
                        oItem.PurchaseItem = BoYesNoEnum.tYES;
                    else if (model.PrchseItem.ToLower() == "n")
                        oItem.PurchaseItem = BoYesNoEnum.tNO;
                }

                if (!string.IsNullOrWhiteSpace(model.SalesItem))
                {
                    if (model.SalesItem.ToLower() == "y")
                        oItem.SalesItem = BoYesNoEnum.tYES;
                    else if (model.SalesItem.ToLower() == "n")
                        oItem.SalesItem = BoYesNoEnum.tNO;
                }

                if (!string.IsNullOrWhiteSpace(model.InventoryItem))
                {
                    if (model.InventoryItem.ToLower() == "y")
                        oItem.InventoryItem = BoYesNoEnum.tYES;
                    else if (model.InventoryItem.ToLower() == "n")
                        oItem.InventoryItem = BoYesNoEnum.tNO;
                }

                oItem.UserFields.Fields.Item("U_IDU_WEBID").Value = model.U_IDU_WEBID;
                oItem.UserFields.Fields.Item("U_IDU_Dimensi").Value = model.Dimensi;
                oItem.UserFields.Fields.Item("U_IDU_NetWeight").Value = model.NetWeight;
                oItem.UserFields.Fields.Item("U_IDU_BoxWeight").Value = model.BoxWeight;
                oItem.UserFields.Fields.Item("U_IDU_StockSafety").Value = model.StockSafety;

                if (oItem.Add() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = "Failed - [AddItem] " + oCompany.GetLastErrorDescription() };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {model.U_IDU_WEBID}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oItem != null)
                {
                    Marshal.ReleaseComObject(oItem);
                    oItem = null;
                }
            }
        }

        public static MessageInfo EditItem(SAPbobsCOM.Company oCompany, ItemModel model)
        {
            string funcname = "EditItem";
            MessageInfo ret = null;
            bool Error = false;
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            Items oItem = null;

            try
            {
                #region Ambil ItemCode
                Qry = $"SELECT \"ItemCode\" FROM OITM WHERE \"U_IDU_WEBID\" = '{model.U_IDU_WEBID.Trim()}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data sales person dengan kode '{model.U_IDU_WEBID.Trim()}' tidak ditemukan" };
                }
                tmp = dt.Rows[0][0].ToString();
                #endregion

                oCompany.StartTransaction();
                oItem = (Items)oCompany.GetBusinessObject(BoObjectTypes.oItems);
                if (!oItem.GetByKey(tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                //oItem.ItemCode = model.ItemCode;
                oItem.ItemName = model.ItemName;

                if (model.PurchaseItem.ToLower() == "y")
                    oItem.PurchaseItem = BoYesNoEnum.tYES;
                else if (model.PurchaseItem.ToLower() == "n")
                    oItem.PurchaseItem = BoYesNoEnum.tNO;

                if (model.SalesItem.ToLower() == "y")
                    oItem.SalesItem = BoYesNoEnum.tYES;
                else if (model.SalesItem.ToLower() == "n")
                    oItem.SalesItem = BoYesNoEnum.tNO;

                if (model.InventoryItem.ToLower() == "y")
                    oItem.InventoryItem = BoYesNoEnum.tYES;
                else if (model.InventoryItem.ToLower() == "n")
                    oItem.InventoryItem = BoYesNoEnum.tNO;

                #region Ambil ItmsGrpCod OITB
                Qry = $"SELECT \"ItmsGrpCod\" FROM OITB WHERE \"U_IDU_WEBID\" = '{model.KdItemGroup}'";
                if (!Function.OpenTable(oCompany, Qry, ref dt, ref tmp))
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };
                }
                if (dt.Rows.Count == 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] Data item group dengan kode web '{model.KdItemGroup}' tidak ada" };
                }
                string itmsgrpcod = dt.Rows[0][0].ToString();
                #endregion

                oItem.ItemsGroupCode = Convert.ToInt32(itmsgrpcod);

                oItem.PurchaseUnitLength = model.Length;
                oItem.PurchaseUnitWidth = model.Width;
                oItem.PurchaseUnitHeight = model.Height;
                oItem.PurchaseUnitVolume = model.Volume;
                oItem.PurchaseUnitWeight = model.GrossWeight;

                oItem.SalesUnitLength = model.Length;
                oItem.SalesUnitWidth = model.Width;
                oItem.SalesUnitHeight = model.Height;
                oItem.SalesUnitVolume = model.Volume;
                oItem.SalesUnitWeight = model.GrossWeight;

                oItem.InventoryWeight = model.GrossWeight;

                if (!string.IsNullOrWhiteSpace(model.PrchseItem))
                {
                    if (model.PrchseItem.ToLower() == "y")
                        oItem.PurchaseItem = BoYesNoEnum.tYES;
                    else if (model.PrchseItem.ToLower() == "n")
                        oItem.PurchaseItem = BoYesNoEnum.tNO;
                }

                if (!string.IsNullOrWhiteSpace(model.SalesItem))
                {
                    if (model.SalesItem.ToLower() == "y")
                        oItem.SalesItem = BoYesNoEnum.tYES;
                    else if (model.SalesItem.ToLower() == "n")
                        oItem.SalesItem = BoYesNoEnum.tNO;
                }

                if (!string.IsNullOrWhiteSpace(model.InventoryItem))
                {
                    if (model.InventoryItem.ToLower() == "y")
                        oItem.InventoryItem = BoYesNoEnum.tYES;
                    else if (model.InventoryItem.ToLower() == "n")
                        oItem.InventoryItem = BoYesNoEnum.tNO;
                }

                //oItem.MinInventory = model.StockMin;
                //oItem.MaxInventory = model.StockMax;

                oItem.UserFields.Fields.Item("U_IDU_WEBID").Value = model.U_IDU_WEBID;
                oItem.UserFields.Fields.Item("U_IDU_Dimensi").Value = model.Dimensi;
                oItem.UserFields.Fields.Item("U_IDU_NetWeight").Value = model.NetWeight;
                oItem.UserFields.Fields.Item("U_IDU_BoxWeight").Value = model.BoxWeight;
                oItem.UserFields.Fields.Item("U_IDU_StockSafety").Value = model.StockSafety;

                if (oItem.Update() != 0)
                {
                    Error = true;
                    return ret = new MessageInfo() { ErrorCode = oCompany.GetLastErrorCode(), Message = $"Failed - [{funcname}] {oCompany.GetLastErrorDescription()}" };
                }
                return ret = new MessageInfo() { ErrorCode = 0, Message = $"Success - [{funcname}] {model.U_IDU_WEBID.Trim()}" };
            }
            catch (Exception ex)
            {
                Error = true;
                return ret = new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.Message.ToString()}" };
            }
            finally
            {
                if (oCompany.InTransaction)
                {
                    if (!Error)
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    else
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                if (oItem != null)
                {
                    Marshal.ReleaseComObject(oItem);
                    oItem = null;
                }
            }
        }

        public static object GetItem(SAPbobsCOM.Company oCompany)
        {
            string funcname = "GetItem";
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            try
            {
                Qry = $"SELECT \"ItemCode\", \"ItemName\", \"PrchseItem\" \"PurchaseItem\", \"SellItem\" \"SalesItem\", \"InvntItem\" \"InventoryItem\", T1.\"U_IDU_WEBID\" \"KdItemGroup\", \"U_IDU_Dimensi\" \"Dimensi\", \"U_IDU_NetWeight\" \"NetWeight\", \"SWeight1\" \"GrossWeight\", \"U_IDU_BoxWeight\" \"BoxWeight\", \"SLength1\" \"Length\", \"SWidth1\" \"Width\", \"SHeight1\" \"Height\", \"SVolume\" \"Volume\", \"U_IDU_StockSafety\" \"StockSafety\", T0.\"U_IDU_WEBID\" FROM OITM T0 LEFT JOIN OITB T1 ON T0.\"ItmsGrpCod\" = T1.\"ItmsGrpCod\" WHERE COALESCE(T0.\"U_IDU_WEBID\",'') <> '' ORDER BY \"ItemCode\"";
                Function.OpenTable(oCompany, Qry, ref dt, ref tmp);
                if (tmp != "")
                    return new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };

                return dt;
            }
            catch (Exception ex)
            {
                return new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
        }

        public static object GetItemByID(SAPbobsCOM.Company oCompany, string ItemCode)
        {
            string funcname = "GetItem";
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();
            try
            {
                Qry = $"SELECT \"ItemCode\", \"ItemName\", \"PrchseItem\" \"PurchaseItem\", \"SellItem\" \"SalesItem\", \"InvntItem\" \"InventoryItem\", T1.\"U_IDU_WEBID\" \"KdItemGroup\", \"U_IDU_Dimensi\" \"Dimensi\", \"U_IDU_NetWeight\" \"NetWeight\", \"SWeight1\" \"GrossWeight\", \"U_IDU_BoxWeight\" \"BoxWeight\", \"SLength1\" \"Length\", \"SWidth1\" \"Width\", \"SHeight1\" \"Height\", \"SVolume\" \"Volume\", \"U_IDU_StockSafety\" \"StockSafety\", T0.\"U_IDU_WEBID\" FROM OITM T0 LEFT JOIN OITB T1 ON T0.\"ItmsGrpCod\" = T1.\"ItmsGrpCod\" WHERE \"ItemCode\" = '{ItemCode}'";
                Function.OpenTable(oCompany, Qry, ref dt, ref tmp);
                if (tmp != "")
                    return new MessageInfo() { ErrorCode = 2, Message = $"Failed - [{funcname}] {tmp}" };

                return dt;
            }
            catch (Exception ex)
            {
                return new MessageInfo() { ErrorCode = 99, Message = $"Failed - [{funcname}] {ex.ToString()}" };
            }
        }
    }
}