﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WebServiceCAT.Models;

namespace WebServiceCAT.SAP
{
    public class Search
    {
        public static object SearchData(SAPbobsCOM.Company oCompany, SearchModel model)
        {
            string Qry = "";
            string tmp = "";
            DataTable dt = new DataTable();

            string selectquery = "";
            string fromquery = "";
            string wherequery = "";
            string orderbyquery = "";

            if (string.IsNullOrEmpty(model.CustomQuery))
            {
                if (!string.IsNullOrEmpty(model.Select))
                {
                    selectquery = "SELECT " + model.Select.Replace("[", "\"").Replace("]", "\"");
                }

                if (!string.IsNullOrEmpty(model.From))
                {
                    fromquery = "FROM " + model.From.Replace("[", "\"").Replace("]", "\"");
                }

                if (!string.IsNullOrEmpty(model.Where))
                {
                    wherequery = "WHERE " + model.Where.Replace("[", "\"").Replace("]", "\"");
                }

                if (!string.IsNullOrEmpty(model.OrderBy))
                {
                    orderbyquery = "ORDER BY " + model.OrderBy.Replace("[", "\"").Replace("]", "\"");
                }

                Qry = string.Join(" ", new string[] { selectquery, fromquery, wherequery, orderbyquery }).Trim();
            }
            else
            {
                if (model.CustomQuery.Split(',')[0] == "GetPaymentTerms")
                {
                    Qry = $"SELECT \"GroupNum\", \"PymntGroup\", \"ExtraDays\" FROM OCTG ORDER BY \"ExtraDays\", \"GroupNum\"";
                }
                else if (model.CustomQuery.Split(',')[0] == "GetOutputTax")
                {
                    Qry = $"SELECT \"Code\", \"Name\", \"Rate\" FROM OVTG WHERE \"Category\" = 'O' AND \"Inactive\" = 'N' ORDER BY \"Rate\"";
                }
                else if (model.CustomQuery.Split(',')[0] == "GetInputTax")
                {
                    Qry = $"SELECT \"Code\", \"Name\", \"Rate\" FROM OVTG WHERE \"Category\" = 'I' AND \"Inactive\" = 'N' ORDER BY \"Rate\"";
                }
                else if (model.CustomQuery.Split(',')[0] == "GetCustomerGroup")
                {
                    Qry = $"SELECT \"GroupCode\", \"GroupName\" FROM OCRG WHERE \"GroupType\" = 'C' ORDER BY \"GroupName\"";
                }
                else if (model.CustomQuery.Split(',')[0] == "GetVendorGroup")
                {
                    Qry = $"SELECT \"GroupCode\", \"GroupName\" FROM OCRG WHERE \"GroupType\" = 'S' ORDER BY \"GroupName\"";
                }
                else if (model.CustomQuery.Split(',')[0] == "GetPPH")
                {
                    Qry = $"SELECT \"WTCode\", \"WTName\" FROM OWHT WHERE \"Inactive\" = 'N' ORDER BY \"WTCode\"";
                }
                else if (model.CustomQuery.Split(',')[0] == "GetDebPayAcct")
                {
                    Qry = $"SELECT \"AcctCode\", \"AcctName\" FROM OACT WHERE \"LocManTran\" = 'Y' ORDER BY \"AcctName\"";
                }
            }
            string errormessage = "";
            Function.OpenTable(oCompany, Qry, ref dt, ref errormessage);
            if (!string.IsNullOrWhiteSpace(errormessage))
                return new MessageInfo() { ErrorCode = 99, Message = $"Failed - [Search] {errormessage}" };
            else
                return dt;
        }
    }
}